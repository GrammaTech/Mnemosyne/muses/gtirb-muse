;;; LSP utilities
(defpackage :gtirb-muse/lsp-utilities
  (:use :jsonrpc
        :jsonrpc/class
        :jsonrpc/connection
        :lsp-server/protocol
        :lsp-server/lsp-server
        :lsp-server/utilities/lsp-utilities
        :gt/full
        :gtirb-muse/path-utilities
        :gtirb-muse/readtable)
  (:import-from :lsp-server.lem-base
                :buffer
                :buffer-start-point
                :buffer-end-point
                :points-to-string)
  (:export :gtirb-muse-running-p
           :show-gtirb-open-error-response))
(in-package :gtirb-muse/lsp-utilities)
(in-readtable gtirb-muse-readtable)

(-> gtirb-muse-running-p (&key (:server lsp-server)) boolean)
(defun gtirb-muse-running-p (&key (server *server*))
  "Return T if SERVER is a running GTIRB muse."
  (not (eq (type-of server) 'lsp-server)))

(-> show-gtirb-open-error-response (string) null)
(defun show-gtirb-open-error-response (response
                                       &aux (*connection* (current-connection)))
  "Show the given error RESPONSE from the LSP client to the user."
  (if (search "ENOENT:" response)
      (show-message (nest (fmt "Could not open gtirb file at ~a")
                          (only-elt)
                          (nth-value 1)
                          (scan-to-strings "open '(.*)'" response))
                    :type :error)
      (show-message response :type :error)))
