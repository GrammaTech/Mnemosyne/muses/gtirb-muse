;;; LSP extensions for GTIRB muse
(defpackage :gtirb-muse/lsp-extensions
  (:use :gt/full
        :lsp-server/protocol
        :gtirb-muse/readtable)
  (:export "+gtirbDecompilation/file/read+"
           "+gtirbDecompilation/file/write+"
           "+gtirbDecompilation/workspace/openFolder+"
           "+gtirbDecompilation/workspace/closeFolderFiles+"
           :+file-provider+
           :+workspace-provider+
           :+gtirb-bed-method-option-table+
           :|FileReadParams|
           :|uri|
           :|encoding|
           :|FileWriteParams|
           :|content|
           :|WorkspaceOpenFolderParams|
           :|WorkspaceCloseFolderFilesParams|))
(in-package :gtirb-muse/lsp-extensions)
(in-readtable gtirb-muse-readtable)

;; Constants for method names.
(defconst +gtirbDecompilation/file/read+
          "gtirbDecompilation/file/read")
(defconst +gtirbDecompilation/file/write+
          "gtirbDecompilation/file/write")
(defconst +gtirbDecompilation/workspace/openFolder+
          "gtirbDecompilation/workspace/openFolder")
(defconst +gtirbDecompilation/workspace/closeFolderFiles+
          "gtirbDecompilation/workspace/closeFolderFiles")

;; Constants for initialization options.
(defconst +file-provider+ "fileProvider")
(defconst +workspace-provider+ "workspaceProvider")

(defconst +gtirb-bed-method-option-table+
  `((,+gtirbDecompilation/file/read+ . ,+file-provider+)
    (,+gtirbDecompilation/file/write+ . ,+file-provider+)
    (,+gtirbDecompilation/workspace/openFolder+ . ,+workspace-provider+)
    (,+gtirbDecompilation/workspace/closeFolderFiles+ . ,+workspace-provider+)))

;; Parameters for custom methods.
(define-interface FileReadParams ()
  (|uri| :type string)
  (|encoding| :type keyword :optional t :initform :|utf-8|))

(define-interface FileWriteParams ()
  (|uri| :type string)
  (|content| :type string)
  (|encoding| :type keyword :optional t :initform :|utf-8|))

(define-interface WorkspaceOpenFolderParams ()
  (|uri| :type string))

(define-interface WorkspaceCloseFolderFilesParams ()
  (|uri| :type string))
