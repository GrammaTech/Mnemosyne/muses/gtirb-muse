;;; GTIRB workspace representation
(defpackage :gtirb-muse/gtirb-workspace
  (:use :cl-base64
        :jsonrpc
        :lsp-server/protocol
        :lsp-server/lsp-server
        :lsp-server/utilities/lsp-utilities
        :gt/full
        :gtirb
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/fitness
        :gtirb-muse/gtirb-utilities
        :gtirb-muse/lsp-extensions
        :gtirb-muse/lsp-utilities
        :gtirb-muse/path-utilities
        :gtirb-muse/readtable)
  (:shadowing-import-from :gtirb :size :symbol)
  (:export :+default-compiler+
           :gtirb-workspace
           :gtirb-path
           :gtirb-object
           :candidate-decompilations
           :disassembly-target
           :upsert-candidate-decompilation
           :remove-candidate-decompilation
           :remove-candidate-decompilations
           :write-gtirb-to-disk
           :write-candidates-to-disk
           :write-candidate-to-disk
           :erase-candidates-from-disk
           :erase-candidate-from-disk))
(in-package :gtirb-muse/gtirb-workspace)
(in-readtable gtirb-muse-readtable)

(defconst +default-compiler+ "timeout 90s clang"
  "Default compilation command for software objects.")

(defclass gtirb-workspace ()
  ((gtirb-path :initarg :gtirb-path :type pathname :reader gtirb-path)
   (gtirb :initarg :gtirb :type gtirb :reader gtirb-object)
   (candidate-decompilations :type hash-table :reader candidate-decompilations)
   (disassembly-target :type hash-table :reader disassembly-target))
  (:documentation "In-memory representation of a GTIRB workspace, with the
GTIRB object and associated parsed candidate decompilations."))

(defmethod initialize-instance :after ((gtirb-workspace gtirb-workspace) &key)
  "Populate the parsed candidate decompilations slot from the GTIRB aux data."
  (let* ((gtirb (gtirb-object gtirb-workspace))
         (ir-sources (gtirb-ir-sources gtirb))
         (ir-sources-ht (if ir-sources
                            (aux-data-data ir-sources)
                            (make-hash-table))))
    (setf (slot-value gtirb-workspace 'candidate-decompilations)
          (iter (for (candidate-name text) in-hashtable ir-sources-ht)
                (collect
                  (cons candidate-name
                        (from-string (make 'c :compiler +default-compiler+)
                                     text))
                  into result)
                (finally (return (alist-hash-table result :test #'equal)))))
    (setf (slot-value gtirb-workspace 'disassembly-target)
          (function-instructions-hashtable gtirb))))

(defgeneric upsert-candidate-decompilation (gtirb-workspace
                                            candidate-name
                                            decompilation)
  (:documentation "Insert/update the given CANDIDATE-NAME DECOMPILATION
in the GTIRB-WORKSPACE.")
  (:method ((gtirb-workspace gtirb-workspace)
            (candidate-name string)
            (text string))
    (upsert-candidate-decompilation gtirb-workspace
                                    candidate-name
                                    (from-string (make 'c) text)))
  (:method ((gtirb-workspace gtirb-workspace)
            (candidate-name string)
            (software c))
    (setf (gethash candidate-name (candidate-decompilations gtirb-workspace))
          software))
  (:method :after ((gtirb-workspace gtirb-workspace)
                   (candidate-name string)
                   (software c))
    (sync-upsert-candidate-decompilation gtirb-workspace candidate-name)))

(defgeneric remove-candidate-decompilation (gtirb-workspace candidate-name)
  (:documentation "Remove the candidate decompilation with CANDIDATE-NAME
from the GTIRB-WORKSPACE.")
  (:method ((gtirb-workspace gtirb-workspace)
            (candidate-name string))
    (remove-candidate-decompilations gtirb-workspace (list candidate-name))))

(defgeneric remove-candidate-decompilations (gtirb-workspace candidate-names)
  (:documentation "Remove the candidate decompilations with CANDIDATE-NAMES
from the GTIRB-WORKSPACE.")
  (:method ((gtirb-workspace gtirb-workspace)
            (candidate-names list))
     (apply #'delete-from-hash-table
            (candidate-decompilations gtirb-workspace)
            candidate-names))
  (:method :after ((gtirb-workspace gtirb-workspace)
                   (candidate-names list))
    (sync-remove-candidate-decompilations gtirb-workspace candidate-names)))

(defgeneric write-gtirb-to-disk (gtirb-workspace)
  (:documentation "Write the in-memory GTIRB representation for the workspace
to disk.")
  (:method ((gtirb-workspace gtirb-workspace))
    (labels ((write-gtirb-to-disk-error-callback (response code)
               "Error callback to display the RESPONSE to the client."
               (declare (ignorable code))
               (show-gtirb-open-error-response response))
             (gtirb-to-base64-string (gtirb)
               "Serialize GTIRB to a base64 string."
               (update-proto gtirb)
               (let* ((size (pb:octet-size (gtirb::proto gtirb)))
                      (buffer (make-array size :element-type '(unsigned-byte 8))))
                 (pb:serialize (gtirb::proto gtirb) buffer 0 size)
                 (usb8-array-to-base64-string buffer))))

      (call-async *server*
                  +gtirbDecompilation/file/write+
                  (make 'FileWriteParams
                        :uri (file-path-uri (gtirb-path gtirb-workspace))
                        :content (nest (gtirb-to-base64-string)
                                       (gtirb-object gtirb-workspace))
                        :encoding :|base64|)
                  nil
                  #'write-gtirb-to-disk-error-callback))))

(defgeneric write-candidates-to-disk (gtirb-workspace &optional candidate-names)
  (:documentation "Write the candidate decompilations for CANDIDATE-NAMES
in the GTIRB-WORKSPACE to disk.")
  (:method ((gtirb-workspace gtirb-workspace) &optional
            (candidate-names (nest (hash-table-keys)
                                   (candidate-decompilations gtirb-workspace))))
    (labels ((candidate-path-and-software (gtirb-workspace candidate-name)
               "Return the path to the decompilation of CANDIDATE-NAME
               in GTIRB-WORKSPACE along with the associated software object."
               (let ((gtirb-path (gtirb-path gtirb-workspace)))
                 (values (candidate-path-from-gtirb-path gtirb-path
                                                         candidate-name)
                         (gethash candidate-name
                                  (candidate-decompilations gtirb-workspace)))))
             (should-write-to-disk (candidate-path sw)
               "Return TRUE if the editor buffer for CANDIDATE-PATH does not
               match the candidate decompilation SW object's source text."
               (not (equal (file-contents-from-buffer candidate-path)
                           (genome-string sw)))))
      (mapc (lambda (candidate-name)
              (multiple-value-bind (candidate-path sw)
                  (candidate-path-and-software gtirb-workspace candidate-name)
                (assert sw (sw) "Candidate decompilation ~a not found"
                                candidate-name)
                (when (should-write-to-disk candidate-path sw)
                  (call-async *server*
                              +gtirbDecompilation/file/write+
                              (make 'FileWriteParams
                                    :uri (file-path-uri candidate-path)
                                    :content (genome-string sw))))))
            candidate-names))))

(defgeneric write-candidate-to-disk (gtirb-workspace candidate-name)
  (:documentation "Write the candidate decompilation for CANDIDATE-NAME
in the GTIRB-WORKSPACE to disk.")
  (:method ((gtirb-workspace gtirb-workspace)
            (candidate-name string))
    (write-candidates-to-disk gtirb-workspace (list candidate-name))))

(defgeneric erase-candidates-from-disk (gtirb-workspace &optional candidate-names)
  (:documentation "Erase the candidate decompilations for CANDIDATE-NAMES
in the GTIRB-WORKSPACE from the disk.")
  (:method ((gtirb-workspace gtirb-workspace) &optional
            (candidate-names (nest (hash-table-keys)
                                   (candidate-decompilations gtirb-workspace))))
    (call-async *server*
                "workspace/applyEdit"
                (nest (make '|ApplyWorkspaceEditParams| :edit)
                      (make '|WorkspaceEdit| :document-changes)
                      (mapcar (lambda (candidate-name)
                                (make '|DeleteFile|
                                      :uri (nest (file-path-uri)
                                                 (candidate-path-from-gtirb-path
                                                  (gtirb-path gtirb-workspace)
                                                  candidate-name))
                                      :options (make '|DeleteFileOptions|
                                                     :ignoreIfNotExists t)))
                              candidate-names)))))

(defgeneric erase-candidate-from-disk (gtirb-workspace candidate-name)
  (:documentation "Erase the candidate decompilation for CANDIDATE-NAME
in the GTIRB-WORKSPACE from the disk.")
  (:method ((gtirb-workspace gtirb-workspace)
            (candidate-name string))
    (erase-candidates-from-disk gtirb-workspace (list candidate-name))))

(-> sync-upsert-candidate-decompilation (gtirb-workspace string) t)
(defun sync-upsert-candidate-decompilation
    (gtirb-workspace candidate-name
     &aux (gtirb (gtirb-object gtirb-workspace)))
  "Sync the CANDIDATE-NAME decompilation update/insertion in the
GTIRB-WORKSPACE with the corresponding GTIRB aux-data tables."
  ;; TODO: too imperative, refactor to a more functional style
  (labels
      ((function-names-hash-table (sw)
         "Return a hashtable mapping function name -> ast for
         each function in SW."
         (alist-hash-table
           (nest (mapcar (lambda (f) (cons (function-name f) f)))
                 (remove-if-not (of-type 'function-ast))
                 (children)
                 (genome sw))
           :test #'equal))
       (sync-function-sources-helper! (candidate-name candidate-functions module)
         "Sync CANDIDATE-NAME's function decompilations (CANDIDATE-FUNCTIONS)
         with the MODULE's function-sources aux data."
         (let ((function-sources (gtirb-function-sources module)))
           (setf (aux-data-data function-sources)
                 (iter (for (uuid sources)
                            in-hashtable (aux-data-data function-sources))
                       (let* ((function-name (gtirb-function-name-for-uuid
                                              gtirb uuid))
                              (function-ast (gethash function-name
                                                     candidate-functions)))
                         (collect
                          (cons uuid
                                (if function-ast
                                    (aux-data-upsert candidate-name
                                                     (source-text function-ast)
                                                     sources)
                                    (aux-data-remove candidate-name sources)))
                          into results))
                       (finally (return (alist-hash-table results)))))))
       (sync-function-sources! (gtirb candidate-name candidate)
         "Sync CANDIDATE-NAME/CANDIDATE's function decompilations with the
         GTIRB module's function-sources aux data."
         (mapc {sync-function-sources-helper!
                candidate-name
                (function-names-hash-table candidate)}
               (modules gtirb)))
       (sync-ir-sources! (gtirb candidate-name candidate
                          &aux (ir-sources (gtirb-ir-sources gtirb)))
         "Sync the CANDIDATE-NAME/CANDIDATE decompilation key-value pair
         with the GTIRB ir-sources aux data."
         (setf (aux-data-data ir-sources)
               (aux-data-upsert candidate-name
                                (genome-string candidate)
                                (aux-data-data ir-sources)))))

    (let ((candidate (nest (gethash candidate-name)
                           (candidate-decompilations gtirb-workspace))))
      (sync-function-sources! gtirb candidate-name candidate)
      (sync-ir-sources! gtirb candidate-name candidate))))

(-> sync-remove-candidate-decompilations (gtirb-workspace list) t)
(defun sync-remove-candidate-decompilations
    (gtirb-workspace candidate-names
     &aux (gtirb (gtirb-object gtirb-workspace)))
  "Sync the deletions of CANDIDATE-NAMES decompilations from GTIRB-WORKSPACE
with the corresponding GTIRB aux-data tables."
  ;; TODO: too imperative, refactor to a more functional style
  (labels
      ((delete-from-aux-data (ht &rest keys)
         "Helper function to delete KEYS from HT."
         (apply #'delete-from-hash-table
                (copy-hash-table ht :test #'equal)
                keys))
       (sync-function-sources-helper! (candidate-names module)
         "Sync the deletion of CANDIDATE-NAMES with the MODULE's
         function-sources aux data."
         (let ((function-sources (gtirb-function-sources module)))
           (setf (aux-data-data function-sources)
                 (iter (for (uuid sources)
                            in-hashtable (aux-data-data function-sources))
                       (collect (cons uuid
                                      (apply #'delete-from-aux-data
                                             sources
                                             candidate-names))
                                into results)
                       (finally (return (alist-hash-table results)))))))
       (sync-function-sources! (gtirb candidate-names)
         "Sync the deletion of CANDIDATE-NAMES with the GTIRB module's
         function-sources aux data."
         (mapc {sync-function-sources-helper! candidate-names}
               (modules gtirb)))
       (sync-ir-sources! (gtirb candidate-names
                          &aux (ir-sources (gtirb-ir-sources gtirb)))
         "Sync the deletion of CANDIDATE-NAMES with the GTIRB ir-sources
         aux data."
         (setf (aux-data-data ir-sources)
               (apply #'delete-from-aux-data
                      (aux-data-data ir-sources)
                      candidate-names))))

    (sync-function-sources! gtirb candidate-names)
    (sync-ir-sources! gtirb candidate-names)))
