;;; LSP server for interacting with GTIRB files with
;;; decompilation data present.
(defpackage :gtirb-muse/server
  (:use :bordeaux-threads
        :lparallel
        :cl-base64
        :cl-store
        :jsonrpc
        :jsonrpc/connection
        :jsonrpc/transport/stdio
        :jsonrpc/transport/tcp
        :lsp-server/protocol
        :lsp-server/protocol-util
        :lsp-server/lsp-server
        :lsp-server/logger
        :lsp-server/utilities/lsp-utilities
        :command-line-arguments
        :gt/full
        :software-evolution-library/command-line
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/utility/range
        :gtirb-muse/bed
        :gtirb-muse/fitness
        :gtirb-muse/gtirb-utilities
        :gtirb-muse/gtirb-workspace
        :gtirb-muse/lsp-extensions
        :gtirb-muse/lsp-utilities
        :gtirb-muse/path-utilities
        :gtirb-muse/readtable)
  (:import-from :flexi-streams
                :make-in-memory-input-stream)
  (:import-from :swank
                :create-server)
  (:import-from :gtirb
                :read-gtirb)
  (:import-from :argot/argot
                :|ArgotFilesParams|
                :+argot/workspace/files+
                :argot-method-option
                :check-argot-option)
  (:import-from :software-evolution-library/utility/debug
                :*note-out*))
(in-package :gtirb-muse/server)
(in-readtable gtirb-muse-readtable)


;;; GTIRB LSP server implementation

(defclass bed-server (lsp-server)
  ((gtirb-workspaces :initarg :gtirb-workspaces
                     :type hash-table
                     :accessor gtirb-workspaces)
   (bed-jobs :initarg :bed-jobs
             :type hash-table
             :accessor bed-jobs))
  (:default-initargs
   :gtirb-workspaces (make-thread-safe-hash-table :test #'equalp)
   :bed-jobs (make-thread-safe-hash-table :test #'equalp))
  (:documentation "LSP server for interacting with decompilation GTIRB files."))

(defconst +bed-server-commands+
  (list :|gtirb.bed.server.start|
        :|gtirb.bed.server.inject-candidate|
        :|gtirb.bed.server.check-status|
        :|gtirb.bed.server.stop|
        :|gtirb.bed.server.clear-results|)
  "List of commands supported by the BED LSP server.")

(defmethod handle ((bed-server bed-server)
                   (method (eql :|initialize|))
                   (params t))
  "Return to the client capabilities of this server, namely code lenses."
  (nest (make 'InitializeResult
              :capabilities)
        (make 'ServerCapabilities
              :textDocumentSync
              (make 'TextDocumentSyncOptions
                    :openClose t
                    :save t
                    :change |TextDocumentSyncKind.Incremental|)
              :codeLensProvider
              (make 'CodeLensOptions :resolveProvider nil)
              :executeCommandProvider
              (make 'ExecuteCommandOptions
                    :commands (mapcar #'string +bed-server-commands+)))))

(defmethod dispatch :around ((bed-server bed-server)
                             (request request))
  "Dispatch processing to the appropriate handler if the client
has the requisite capabilities for the BED server."
  (when (or (equal "initialize" (request-method request))
            (server-enabled?))
    (call-next-method)))

;; The following code allows for opening/closing of GTIRB workspace
;; folders in VSCode in a user-friendly manner.  Because closing of
;; a workspace in VSCode causes shutdown and reinitialization of
;; the LSP connection, we need to keep track of state across multiple
;; LSP operations to determine if/where we are in the opening/closing
;; process.  This necessitates the below code to fit a square peg
;; into a round hole.
(let ((opening-workspace-counter (cons 0 nil))
      (closing-workspace-counter (cons 0 nil)))

  (defmethod handle :before ((bed-server bed-server)
                             (method (eql :|textDocument/didOpen|))
                             (params t))
    "Increment the opening-workspace-counter when we initiate
    opening a new GTIRB workspace."
    (when (and (zerop-counter opening-workspace-counter)
               (zerop-counter closing-workspace-counter))
      (incf-counter opening-workspace-counter)))

  (defmethod handle :before ((bed-server bed-server)
                             (method symbol)
                             (params t))
    "Decrement the opening/closing-workspace-counter when we are
    finished opening/closing a GTIRB workspace."
    (unless (member method '(:|textDocument/didOpen| :|shutdown|
                             :|initialize| :|initialized|))
      (decf-counter opening-workspace-counter)
      (decf-counter closing-workspace-counter)))

  (defmethod handle :around ((bed-server bed-server)
                             (method (eql :|textDocument/didOpen|))
                             (params t))
    "Disallow calling the didOpen handler when we are closing a GTIRB
    workspace.  This prevents us from re-opening a GTIRB workspace
    while we are closing it."
    (when (zerop-counter closing-workspace-counter)
      (call-next-method)))

  (defmethod handle :before ((bed-server bed-server)
                             (method (eql :|initialized|))
                             (params t))
    "Handle closing of GTIRB workspaces.  This is performed when
    reinitializing a new session as all LSP messages are dropped
    after shutdown is performed.  Clear all GTIRB workspaces in
    memory to allow for reloading of potentially changed GTIRB
    files and close all files not associated with the current
    workspace (if such files are open)."
    (when (zerop-counter opening-workspace-counter)
      (iter (for gtirb-path in (hash-table-keys (gtirb-workspaces bed-server)))
            (and-let* ((cache-path (cache-dir-from-gtirb-path gtirb-path))
                       ((not (equalp cache-path (root-path)))))
              (incf-counter closing-workspace-counter)
              (call-async bed-server
                          +gtirbDecompilation/workspace/closeFolderFiles+
                          (nest (make 'WorkspaceCloseFolderFilesParams :uri)
                                (file-path-uri cache-path))
                          {decf-counter closing-workspace-counter}
                          {decf-counter closing-workspace-counter})))
      (clear-gtirb-workspaces bed-server)))

  (-> root-path (&optional (or InitializeParams null))
                (values (or pathname null) &optional))
  (defun root-path (&optional (params (initialize-params-safe)))
    "Return the ROOT-URI in this LSP session's initialization params as a
    pathname object."
    (match params
      ((InitializeParams :root-uri uri)
       (and uri (ensure-directory-pathname (file-uri-path uri))))
      (_ nil)))

  (-> zerop-counter (cons) boolean)
  (defun zerop-counter (counter)
    "Return T if counter is zero."
    (zerop (car counter)))

  (-> incf-counter (cons &rest t) fixnum)
  (defun incf-counter (counter &rest args)
    "Increment the value of counter."
    (declare (ignorable args))
    (incf (car counter)))

  (-> decf-counter (cons &rest t) fixnum)
  (defun decf-counter (counter &rest args)
    "Decrement the value of counter to a minimum value of zero (inclusive)."
    (declare (ignorable args))
    (if (zerop-counter counter)
        (car counter)
        (decf (car counter)))))

(defun server-enabled? ()
  "Return T if the client has the capabilities required for the BED server."
  (and (argot-methods-supported-p)
       (gtirb-bed-methods-supported-p)
       (workspace-resource-operations-supported-p)
       (which +gtirb-objdump+)
       (which +ddisasm+)))

(-> argot-methods-supported-p () (values boolean &optional))
(defun argot-methods-supported-p ()
  "Return T if the argot methods required by this server are supported
by the client."
  (nest (every #'get-argot-init-option)
        (mapcar (lambda (method) (argot-method-option method))
                (list +argot/workspace/files+))))

(-> get-argot-init-option (string) t)
(defun get-argot-init-option (key)
  "Retrieve the data in the argot initialization params for the given KEY."
  (check-argot-option key)
  (gethash key (get-init-options "argot")))

(-> gtirb-bed-methods-supported-p () (values boolean &optional))
(defun gtirb-bed-methods-supported-p ()
  "Return T if the custom methods required by the LSP server are
supported by the client."
  (nest (every {gtirb-bed-method-supported-p})
        (mapcar #'car +gtirb-bed-method-option-table+)))

(-> gtirb-bed-method-supported-p (string) (values boolean &optional))
(defun gtirb-bed-method-supported-p (method)
  "Return T if the given METHOD is supported by the client as specified
in the initialization params."
  (nest (gtirb-bed-init-option)
        (assocdr method +gtirb-bed-method-option-table+ :test #'equal)))

(-> gtirb-bed-init-option (string) t)
(defun gtirb-bed-init-option (key)
  "Retrieve the data in the gtirbDecompilation initialization params for the
given KEY."
  (gethash key (get-init-options "gtirbDecompilation")))

(-> get-init-options (string &optional (or InitializeParams null)) hash-table)
(defun get-init-options (key &optional (params (initialize-params-safe)))
   (match params
     ((InitializeParams :initialization-options (and options (type hash-table)))
      (gethash key options))
     (_ (make-hash-table))))

(-> workspace-resource-operations-supported-p () boolean)
(defun workspace-resource-operations-supported-p ()
  "Return T if the workspace resource operations required by the BED server
are supported by the client."
  (match (initialize-params-safe)
    ((InitializeParams
      :capabilities
      (ClientCapabilities
       :workspace
       (WorkspaceClientCapabilities
        :workspace-edit
        (WorkspaceEditClientCapabilities
         :resourceOperations resource-operations))))
     (equalp (list "create" "rename" "delete") resource-operations))))


;;; Management of GTIRB workspaces

(defgeneric get-gtirb-workspace (bed-server gtirb-path)
  (:documentation "Return the workspace for the GTIRB file at GTIRB-PATH.")
  (:method ((bed-server bed-server)
            (gtirb-path pathname))
    (gethash gtirb-path (gtirb-workspaces bed-server))))

(defgeneric add-gtirb-workspace (bed-server gtirb-path gtirb-workspace)
  (:documentation "Add the given GTIRB-PATH/WORKSPACE pair to the cache.")
  (:method ((bed-server bed-server)
            (gtirb-path pathname)
            (gtirb-workspace gtirb-workspace))
    (setf (gethash gtirb-path (gtirb-workspaces bed-server)) gtirb-workspace)))

(defgeneric remove-gtirb-workspace (bed-server gtirb-path)
  (:documentation "Remove the workspace for the GTIRB file at GTIRB-PATH.")
  (:method ((bed-server bed-server)
            (gtirb-path pathname))
    (remhash gtirb-path (gtirb-workspaces bed-server)))
  (:method :after ((bed-server bed-server)
                   (gtirb-path pathname))
    (remove-bed-job bed-server gtirb-path)))

(defgeneric clear-gtirb-workspaces (bed-server)
  (:documentation "Clear the workspace cache.")
  (:method ((bed-server bed-server))
    (iter (for gtirb-path in (hash-table-keys (gtirb-workspaces bed-server)))
          (remove-gtirb-workspace bed-server gtirb-path))))


;;; BED commands

(defmethod handle ((bed-server bed-server)
                   (method (eql :|workspace/executeCommand|))
                   (params hash-table))
  "Dispatch execution of the command to the appropriate handler."
  (execute-command bed-server
                   (make-keyword (gethash "command" params))
                   (gethash "arguments" params)))

(defgeneric execute-command (bed-server command arguments)
  (:documentation "Execute BED-SERVER's COMMAND with ARGUMENTS."))

(defmethod execute-command :around ((bed-server bed-server)
                                    (command symbol)
                                    (arguments list))
  "Sanity check that a GTIRB workspace is open before attempting
to execute the COMMAND."
  (declare (ignorable command))
  (if-let* ((gtirb-path (gtirb-path-from-arguments arguments))
            (gtirb-workspace (get-gtirb-workspace bed-server gtirb-path)))
    (call-next-method)
    (show-message "No GTIRB workspace associated with the current file."
                  :type :error)))

(defmethod execute-command ((bed-server bed-server)
                            (command (eql :|gtirb.bed.server.start|))
                            (arguments list)
                            &aux (gtirb-path (gtirb-path-from-arguments
                                              arguments)))
  "Start the BED process for the current workspace."
  (show-message "Starting decompilation evolutionary process.")
  (nest (add-bed-job bed-server gtirb-path)
        (apply #'make 'bed-job
               :gtirb-workspace (get-gtirb-workspace bed-server gtirb-path)
               :threaded t)
        (translate-start-command-arguments)
        (cdr arguments)))

(defmethod execute-command ((bed-server bed-server)
                            (command (eql :|gtirb.bed.server.inject-candidate|))
                            (arguments list))
  "Inject a candidate decompilation into the population of the BED
process for the current workspace."
  (let* ((job (bed-job-from-arguments bed-server arguments))
         (message (and job (translate-inject-command-arguments job arguments))))
    (cond ((not (and job (eq (job-status job) :job-running)))
           (show-message "No decompilation job currently running."
                         :type :error))
          ((null message)
           (show-message "Current file cannot be injected into the population."
                         :type :error))
          (t (push-message-queue job message)))))

(defmethod execute-command ((bed-server bed-server)
                            (command (eql :|gtirb.bed.server.check-status|))
                            (arguments list))
  "Show the status of the BED process for the current workspace."
  (if-let ((job (bed-job-from-arguments bed-server arguments)))
    (show-bed-job-status job)
    (show-message "No decompilation job started." :type :error)))

(defmethod execute-command ((bed-server bed-server)
                            (command (eql :|gtirb.bed.server.stop|))
                            (arguments list))
  "Stop the BED process for the current workspace."
  (let ((job (bed-job-from-arguments bed-server arguments)))
    (if (and job (eq (job-status job) :job-running))
        (stop-bed-job job)
        (show-message "No decompilation job currently running." :type :error))))

(defmethod execute-command ((bed-server bed-server)
                            (command (eql :|gtirb.bed.server.clear-results|))
                            (arguments list))
  "Clear the BED results for the current workspace."
  (let* ((gtirb-workspace (nest (get-gtirb-workspace bed-server)
                                (gtirb-path-from-arguments arguments)))
         (job (bed-job-from-arguments bed-server arguments)))
    (if (and job (eq (job-status job) :job-running))
        (show-message "Cannot clear decompilation results while job is running."
                      :type :error))
        (progn
          (clear-bed-results gtirb-workspace)
          (erase-population-from-disk gtirb-workspace)
          (write-gtirb-to-disk gtirb-workspace))))

(-> gtirb-path-from-arguments (list)
                              (values (or pathname null) &optional))
(defun gtirb-path-from-arguments (arguments)
  "Return the GTIRB file associated with the given list of
command ARGUMENTS."
  (gtirb-path-from-uri (first arguments)))

(-> bed-job-from-arguments (bed-server list)
                           (values (or null bed-job) &optional))
(defun bed-job-from-arguments (bed-server arguments)
  "Return the BED job associated with the given list of
command ARGUMENTS."
  (when-let ((gtirb-path (gtirb-path-from-arguments arguments)))
    (get-bed-job bed-server gtirb-path)))

(-> translate-start-command-arguments (list) list)
(defun translate-start-command-arguments (arguments)
  "Translate the given list of ARGUMENTS to the :|gtirb.bed.server.start|
command to keyword arguments for a BED job."
  (destructuring-bind (population max-generations test-script test-case-timeout)
      arguments
    (list :population (unless (emptyp population)
                        (nest (restore)
                              (make-in-memory-input-stream)
                              (base64-string-to-usb8-array population)))
          :max-generations (if (< max-generations 0)
                               infinity
                               max-generations)
          :test-script (if (emptyp test-script) nil test-script)
          :test-case-timeout (if (< test-case-timeout 0)
                                 +default-test-case-timeout+
                                 test-case-timeout))))

(-> translate-inject-command-arguments (bed-job list) list)
(defun translate-inject-command-arguments (job arguments)
  "Translate the given list of ARGUMENTS to the
:|gtirb.bed.server.inject-candidate| command to a message
to pass to the BED JOB."
  (destructuring-bind (uri num-copies) arguments
    (and-let* ((path (file-uri-pathname uri))
               ((candidate-path-p path))
               (candidate (nest (gethash (candidate-name-from-path path))
                                (candidate-decompilations)
                                (gtirb-workspace job))))
      (list :inject candidate num-copies))))


;;; Management of BED jobs

(defgeneric get-bed-job (bed-server gtirb-path)
  (:documentation "Return the BED job for the GTIRB file at GTIRB-PATH,
if such a job exists.")
  (:method ((bed-server bed-server)
            (gtirb-path pathname))
    (gethash gtirb-path (bed-jobs bed-server))))

(defgeneric add-bed-job (bed-server gtirb-path job)
  (:documentation "Add a BED JOB for the GTIRB file at GTIRB-PATH.")
  (:method ((bed-server bed-server)
            (gtirb-path pathname)
            (job bed-job))
    (setf (gethash gtirb-path (bed-jobs bed-server)) job)))

(defgeneric remove-bed-job (bed-server gtirb-path)
  (:documentation "Remove the BED job for the GTIRB file at GTIRB-PATH,
if such a job exists.")
  (:method :before ((bed-server bed-server)
                    (gtirb-path pathname))
    (when-let ((job (get-bed-job bed-server gtirb-path)))
      (stop-bed-job job)))
  (:method ((bed-server bed-server)
            (gtirb-path pathname))
    (remhash gtirb-path (bed-jobs bed-server))))


;;; Opening a gtirb workspace

(defmethod handle ((bed-server bed-server)
                   (method (eql :|textDocument/didOpen|))
                   (params |DidOpenTextDocumentParams|))
  "Open a GTIRB workspace if a candidate decompilation or ASM listing
is opened by the user."
  (call-next-method)
  (and-let* ((path (gtirb-path-from-params params))
             ((null (get-gtirb-workspace bed-server path))))
    (open-gtirb path)))

(-> open-gtirb (pathname) (values null &optional))
(defun open-gtirb (gtirb-path)
  "Request the contents of a GTIRB file at GTIRB-PATH from the client
and, if the read is successful, open the GTIRB workspace."
  (call-async *server*
              +gtirbDecompilation/file/read+
              (make 'FileReadParams :uri (file-path-uri gtirb-path)
                                    :encoding :|base64|)
              #'open-gtirb-callback
              #'open-gtirb-error-callback))

(-> open-gtirb-callback (hash-table) t)
(defun open-gtirb-callback (response &aux (*connection* (current-connection)))
  "Read in the GTIRB file from the client and prepare the server and editor."
  (and-let* ((gtirb-path (pathname (file-uri-path (gethash "uri" response))))
             (gtirb-workspace (nest (make 'gtirb-workspace
                                          :gtirb-path gtirb-path
                                          :gtirb)
                                    (read-gtirb)
                                    (base64-string-to-usb8-array)
                                    (gethash "content" response)))
             ((hash-table-alist (candidate-decompilations gtirb-workspace))))
    (add-gtirb-workspace *server* gtirb-path gtirb-workspace)
    (open-cache-dir-in-workspace gtirb-path)
    (write-candidates-to-disk gtirb-workspace)))

(-> open-cache-dir-in-workspace (pathname) (values null &optional))
(defun open-cache-dir-in-workspace (gtirb-path)
  "Open the cache directory associated with the GTIRB file at GTIRB-PATH
in the client editor."
  (call-async *server*
              +gtirbDecompilation/workspace/openFolder+
              (nest (make 'WorkspaceOpenFolderParams :uri)
                    (file-path-uri)
                    (cache-dir-from-gtirb-path gtirb-path))))

(-> open-gtirb-error-callback (string number) (values null &optional))
(defun open-gtirb-error-callback (response code)
  "Display the error response given by the LSP CLIENT to the user."
  (declare (ignorable code))
  (show-gtirb-open-error-response response))


;;; Saving candidate decompilation to gtirb

(defmethod handle ((bed-server bed-server)
                   (method (eql :|textDocument/didSave|))
                   (params |DidSaveTextDocumentParams|))
  "Save the candidate decompilation to the GTIRB file on disk."
  (call-next-method)
  (when-let* ((path (candidate-path-from-params params)))
    (nest (write-gtirb-to-disk)
          (get-gtirb-workspace bed-server)
          (gtirb-path-from-candidate-path path))))

(defmethod handle :around ((bed-server bed-server)
                           (method (eql :|textDocument/didSave|))
                           (params |DidSaveTextDocumentParams|))
  "Sync the candidate decompilation with the corresponding GTIRB aux data
before saving the GTIRB file to disk."
  (if (asm-view-path-from-params params)
      (show-message "GTIRB rewriting disabled in decompilation mode."
                    :type :error)
      (and-let* ((path (candidate-path-from-params params))
                 (gtirb-workspace (nest (get-gtirb-workspace bed-server)
                                        (gtirb-path-from-candidate-path path)))
                 (candidate-name (candidate-name-from-path path))
                 (candidate (from-string (make 'c :compiler +default-compiler+)
                                         (file-contents-from-buffer path))))
        (if (contains-error-ast-p (genome candidate))
            (nest (show-error-ast-message candidate)
                  (contains-error-ast-p)
                  (genome candidate)))
            (progn
              (upsert-candidate-decompilation gtirb-workspace
                                              candidate-name
                                              candidate)
              (call-next-method)))))

(defmethod handle :after ((bed-server bed-server)
                          (method (eql :|textDocument/didSave|))
                          (params |DidSaveTextDocumentParams|))
  "Refresh code lenses after saving out the document."
  (call-async bed-server "workspace/codeLens/refresh"))

(-> show-error-ast-message (c c-ast) null)
(defun show-error-ast-message (candidate error-ast)
  "Show an error message to the client with the location of the parse
ERROR-AST."
  (labels
      ((leading-ws (line)
         "Return the amount of leading whitespace for LINE."
         (- (length line) (length (drop-while #'whitespacep line))))
       (adjust-source-location (ast location)
         "Return the location of AST, adjusted for display to the client
         with leading whitespace removed and 1-indexed."
         (iter (for line in (lines (string+ (before-text ast) (source-text ast))))
               (for index upfrom 0)
               (while (blankp line))
               (finally (return (make 'source-location
                                  :line (+ index (line location))
                                  :column (+ (leading-ws line)
                                             (if (zerop index)
                                                 (column location)
                                                 1))))))))
    (when-let* ((location (nest (adjust-source-location error-ast)
                                (ast-start candidate error-ast))))
      (show-message (fmt "Could not parse candidate starting at ~d:~d"
                         (line location) (column location))
                    :type :error))))


;;; Code lens

(defconst +code-lenses-init-key+ "codeLenses")
(defconst +peek-view-command+ "client.peekView")
(defparameter +globl-asm-re+
              (create-scanner "\\.globl ([A-Za-z0-0_]+)"))
(defparameter +type-asm-re+
              (create-scanner "\\.type ([A-Za-z0-9_]+), @function"))
(defparameter +function-label-re+
              (create-scanner "([A-Za-z0-9_]+):"))

(defmethod handle ((bed-server bed-server)
                   (method (eql :textDocument/codeLens))
                   (params |CodeLensParams|))
  "Display a codelens with a peek-view of candidate decompilations in the
ASM listing or candidate decompilation byte equivalence information."
  (let ((path (path-from-params params)))
    (cond ((asm-view-path-p path)
           (create-asm-view-code-lenses bed-server path))
          ((candidate-path-p path)
           (create-candidate-code-lenses bed-server path)))))

(-> create-asm-view-code-lenses (bed-server pathname) list)
(defun create-asm-view-code-lenses (bed-server path)
  "Create code lenses for the ASM view file at PATH."
  (and-let* (((show-decompilation-code-lenses-p))
             (lines (coerce (nest (split-sequence #\Newline)
                                  (file-contents-from-buffer path))
                            'vector))
             (gtirb-workspace (nest (get-gtirb-workspace bed-server)
                                    (gtirb-path-from-asm-view-path path))))
    (iter (for index upfrom 0)
          (for line in-vector lines)
          (and-let* (((asm-function-definition-p index lines))
                     (fname (parse-asm-function-name line))
                     (code-lens (make-peek-view-lens gtirb-workspace
                                                     fname
                                                     (+ index 3))))
            (collect code-lens)))))

(-> show-decompilation-code-lenses-p () boolean)
(defun show-decompilation-code-lenses-p ()
  (nest (gethash "decompilations")
        (gtirb-bed-init-option +code-lenses-init-key+)))

(-> asm-function-definition-p (number vector) t)
(defun asm-function-definition-p (index lines)
  "Return T if the INDEX into LINES represents the start of a function
definition in the ASM listing."
  (and (length< (+ index 4) lines)
       (blankp (elt lines (+ index 4)))
       (starts-with-subseq "#" (elt lines (+ index 2)))
       (scan +globl-asm-re+ (elt lines index))
       (scan +type-asm-re+ (elt lines (+ index 1)))
       (scan +function-label-re+ (elt lines (+ index 3)))))

(-> parse-asm-function-name (string) (values string &optional))
(defun parse-asm-function-name (line)
  "Parse the function name from the ASM listing LINE."
  (first (nth-value 1 (scan-to-strings +globl-asm-re+ line))))

(let ((cache (make-thread-safe-hash-table))
      (cache-size 256))
  (-> function-name-lineno-hashtable (c) (values hash-table &optional))
  (defun function-name-lineno-hashtable (sw &aux (root (genome sw)))
    "Return a hash-table mapping function name -> lineno for each function in SW."
    ;; Cache management - randomly drop half the entries if the cache gets
    ;; too large.
    (when (> (hash-table-count cache) cache-size)
      (nest (apply #'delete-from-hash-table cache)
            (drop (floor (/ cache-size 2)))
            (shuffle)
            (hash-table-keys cache)))

    ;; Retrieve the result from the cache or add it to the cache.
    (or (gethash root cache)
        (setf (gethash root cache)
              (alist-hash-table
                (nest (mapcar (lambda (pair)
                                (cons (nest (source-text)
                                            (c/cpp-declarator)
                                            (car pair))
                                      (1- (line (begin (cdr pair)))))))
                      (remove-if-not [{find-if (of-type 'function-ast)}
                                      {get-parent-asts* root} #'car])
                      (remove-if-not [(of-type 'c/cpp-function-declarator)
                                      #'car])
                      (ast-source-ranges root))
                :test #'equal)))))

(-> make-peek-view-lens (gtirb-workspace string number)
                        (values (or CodeLens null) &optional))
(defun make-peek-view-lens (gtirb-workspace fname asm-lineno)
  "Create a code lens to display candidate decompilations in the
GTIRB-WORKSPACE for the function FNAME at the ASM-LINENO in the
ASM listing."
  (labels ((make-title (fname)
             "Create the title of the code lens for the function FNAME."
             (fmt "Candidate decompilations (~a)" fname))
           (candidate-decompilation-locs (gtirb-workspace fname)
             "Return a list of the locations of the candidate decompilations
             of FNAME in GTIRB-WORKSPACE for display in the peek view."
             (iter (for (candidate-name sw) in-hashtable
                        (candidate-decompilations gtirb-workspace))
                   (for ht = (function-name-lineno-hashtable sw))
                   (when-let ((lineno (gethash fname ht))
                              (path (candidate-path-from-gtirb-path
                                     (gtirb-path gtirb-workspace)
                                     candidate-name)))
                     (nest (collect)
                           (alist-hash-table)
                           (list (cons "uri" (file-path-uri path)))
                                 (cons "position" (make-position lineno)))))))

    (when-let ((asm-position (make-position asm-lineno))
               (locs (candidate-decompilation-locs gtirb-workspace fname)))
      (make 'CodeLens
        :range (make-empty-range asm-position)
        :command (make 'Command
                   :title (make-title fname)
                   :command +peek-view-command+
                   :arguments (cons asm-position locs))))))

(-> create-candidate-code-lenses (bed-server pathname) (values list &optional))
(defun create-candidate-code-lenses (bed-server path)
  "Create code lenses for the candidate decompilation at PATH."
  (and-let* (((show-fitness-code-lens-p))
             (gtirb-workspace (nest (get-gtirb-workspace bed-server)
                                    (gtirb-path-from-candidate-path path)))
             (candidate (gethash (candidate-name-from-path path)
                                 (candidate-decompilations gtirb-workspace)))
             (*disassembly-target* (disassembly-target gtirb-workspace)))
    (evaluate-threadsafe #'disassembly-test candidate)
    (nest (remove-if #'null)
          (cons (create-behavioral-fitness-code-lens candidate)
                (create-disassembly-fitness-code-lens candidate)))))

(-> show-fitness-code-lens-p () boolean)
(defun show-fitness-code-lens-p ()
  "Return non-nil if we should show the each function's fitness as a code lens."
  (nest (gethash "fitness")
        (gtirb-bed-init-option +code-lenses-init-key+)))

(-> create-behavioral-fitness-code-lens (c) (or CodeLens null))
(defun create-behavioral-fitness-code-lens (candidate)
  "Create a code lens for the behavioral fitness of CANDIDATE."
  (and-let* ((fitness (behavioral-fitness candidate))
             ((length> fitness 0)))
    (nest (make 'CodeLens :range (make-empty-range (make-position 0))
                          :command)
          (make 'Command :command "" :title)
          (fmt "Passing Tests: ~d/~d"
               (count-if-not #'zerop fitness) (length fitness)))))

(-> create-disassembly-fitness-code-lens (c) list)
(defun create-disassembly-fitness-code-lens (candidate)
  "Create code lenses for the disassembly fitness of CANDIDATE."
  (cons (nest (make 'CodeLens :range (make-empty-range (make-position 0))
                              :command)
              (make 'Command :command "" :title)
              (fmt "Byte equivalence: ~,2f%"
                   (* 100 (mean (disassembly-fitness candidate)))))
        (iter (iter:with fitness-hashtable =
                         (function-fitness-hashtable candidate))
              (for (fname lineno)
                   in-hashtable (function-name-lineno-hashtable candidate))
              (when-let ((fitness (gethash fname fitness-hashtable)))
                (collect (make 'CodeLens
                           :range (make-empty-range (make-position lineno))
                           :command (make 'Command
                                      :title (fmt "Byte equivalence: ~,2f%"
                                                  (* 100 fitness))
                                      :command "")))))))

(-> function-fitness (c) (values hash-table &optional))
(defun function-fitness-hashtable (candidate)
  "Return a hashtable mapping function name -> fitness for CANDIDATE."
  (alist-hash-table (map 'list #'cons
                         (hash-table-keys *disassembly-target*)
                         (disassembly-fitness candidate))
                    :test #'equal))


;;; Command line for starting the LSP server

(defconst +default-num-threads+ (floor (count-cpus) 4))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defparameter +command-line-options+
    `((("help" #\h #\?) :type boolean :optional t
       :documentation "display help output")
      (("log-file" #\L) :type string
       :documentation
       "append log to file, \"STDOUT\" or \"-\" for terminal, 2 for stderr")
      (("no-log" #\n) :type boolean :optional t
       :documentation "inhibit logging")
      (("mode" #\m) :type string :initial-value "tcp"
       :documentation "set server mode to \"stdio\" or \"tcp\"")
      (("port" #\p) :type integer :initial-value 9988
       :documentation "port used in TCP mode")
      (("num-threads" #\n) :type integer :initial-value ,+default-num-threads+
       :documentation "number of threads to utilize")
      (("swank" #\s) :type integer
                     :action #'handle-swank-port-argument-and-set-interactive
                     :documentation "start a swank listener on PORT")
      (("read-seed") :type string
       :action #'handle-read-random-state-from-path-argument
       :documentation "load random seed from FILE")
      (("save-seed") :type string
       :action #'handle-save-random-state-to-path-argument
       :documentation "save random seed to FILE")
      (("verbose" #\V) :type integer :initial-value 2
                       :action #'handle-set-verbose-argument
                       :documentation "verbosity level 0-4"))))

(defun make-server-kernel (&optional (num-threads +default-num-threads+))
  "Make a kernel with NUM-THREAD workers."
  (make-kernel num-threads
               :name "BED server kernel"
               :bindings `((*standard-output* . ,*standard-output*)
                           (*error-output* . ,*error-output*)
                           (*trace-output* . ,*trace-output*)
                           (*server* . ,*server*)
                           (*note-out* . ,*note-out*))))

(define-command server (&spec +command-line-options+
                        &aux (log-file (parse-log-file-options log-file no-log)))
  "Start the GTIRB LSP server."
  #.(format nil "~%Built with ~a version ~a.~%"
            (lisp-implementation-type)
            (lisp-implementation-version))
  (declare (ignorable verbose))
  (when help (show-help-for-server) (quit))
  (setf *server* (make 'bed-server))
  (with-log-file (log-file)
    (let* ((mode (make-keyword (string-upcase mode))))
      (setf *note-out* *logger-stream*)
      (setf (logger-stream *server*) *logger-stream*)
      (setf *kernel* (make-server-kernel num-threads))
      (when (and (eql log-file t) (eql mode :stdio))
        (error "Cannot use stdio transport and log to stdout."))
      (ecase mode
        (:tcp (format *logger-stream* "mode: tcp~%port: ~D~%" port))
        (:stdio (format *logger-stream* "mode: stdio~%")))

      (with-address-in-use-handling (:interactive-p *lisp-interaction*)
        (with-interrupt-shutdown (:error-output *logger-stream*)
          (server-listen *server*
                         :allow-other-keys t
                         :mode mode
                         :host "0.0.0.0"
                         :port port)
          (when swank (wait-on-swank))
          (exit-command server 0 *server*))))))
