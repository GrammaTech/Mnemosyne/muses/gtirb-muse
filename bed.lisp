;;; Behavioral Equivalent Decompilation (BED) implementation
(defpackage :gtirb-muse/bed
  (:use :bordeaux-threads
        :lparallel
        :cl-base64
        :cl-store
        :jsonrpc
        :lsp-server/protocol
        :lsp-server/lsp-server
        :lsp-server/utilities/lsp-utilities
        :lsp-server/utilities/worker-thread
        :gt/full
        :software-evolution-library/software-evolution-library
        :software-evolution-library/components/formatting
        :software-evolution-library/components/lexicase
        :software-evolution-library/components/test-suite
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/utility/debug
        :gtirb-muse/crossover
        :gtirb-muse/fitness
        :gtirb-muse/gtirb-workspace
        :gtirb-muse/gtirb-utilities
        :gtirb-muse/lsp-extensions
        :gtirb-muse/lsp-utilities
        :gtirb-muse/minimize
        :gtirb-muse/mutations
        :gtirb-muse/path-utilities
        :gtirb-muse/readtable)
  #+sbcl
  (:import-from :fset
                :*sbcl-next-serial-number*)
  (:import-from :flexi-streams
                :make-in-memory-output-stream
                :get-output-stream-sequence)
  (:import-from :uiop/driver
                :print-backtrace)
  (:import-from :yason
                :encode)
  (:export :*crossover-seed-chance*
           :bed-job
           :job-status
           :show-bed-job-status
           :stop-bed-job
           :push-message-queue
           :clear-bed-results
           :write-population-to-disk
           :erase-population-from-disk))
(in-package :gtirb-muse/bed)
(in-readtable gtirb-muse-readtable)

(defconst +bed-prefix+ "bed")

(defconst +mutations+
  (cumulative-distribution
   (normalize-probabilities
    '((tree-sitter-insert      . 10)
      (tree-sitter-swap        . 10)
      (tree-sitter-move        . 10)
      (tree-sitter-replace     . 10)
      (tree-sitter-cut         . 10)
      (remove-unused-parameter . 17)
      (bind-callsite-return    . 17)
      (non-void-return         . 17)))))

(defvar *crossover-seed-chance* 0.75
  "Fraction of initial population seeds created by crossover.")

(deftype bed-job-status ()
  "Enumeration of BED job statuses."
  '(member :job-running :job-finished :job-terminated))

(defclass bed-job ()
  ((gtirb-workspace :type gtirb-workspace
                    :initarg :gtirb-workspace
                    :reader gtirb-workspace)
   (message-queue :type queue
                  :initform (queue)
                  :reader message-queue)
   (job-status :type bed-job-status
               :initform :job-running
               :reader job-status)
   (worker-thread :initform nil :type (or null thread) :reader worker-thread)
   (population :initform nil :type list :reader population)
   (best-candidate :initform nil :type (or null c) :reader best-candidate)
   (fitness-evals :initform 0 :type (or null number) :reader fitness-evals))
  (:documentation "GTIRB/BED evolutionary process wrapper."))

(defmethod encode ((job bed-job) &optional stream)
  "Implement stub JSON encode for BED jobs."
  (write-string "null" stream)
  job)

(defgeneric push-message-queue (job message)
  (:documentation "Push MESSAGE onto the message queue for JOB.")
  (:method ((job bed-job) (message t))
    (with-slots (message-queue) job
      (synchronized (message-queue)
        (enq message message-queue)
        nil))))

(defgeneric pop-message-queue (job)
  (:documentation "Pop a MESSAGE from the message for JOB.")
  (:method ((job bed-job))
    (with-slots (message-queue) job
      (synchronized (message-queue)
        (deq message-queue)))))

(defmethod initialize-instance :after ((job bed-job)
                                       &rest initargs &key &allow-other-keys)
  "Start the BED process in a seperate thread after initialization."
  (if (not (plist-get :threaded initargs))
      (apply #'bed job initargs)
      (setf (slot-value job 'worker-thread)
            (let* ((ready nil)
                   (thread
                    (with-lsp-worker-thread (:name (nest (fmt "bed@~a")
                                                         (file-namestring)
                                                         (gtirb-path)
                                                         (gtirb-workspace job)))
                      (setf ready t)
                      (apply #'bed job initargs))))
              ;; Wait until the thread is actually running before returning.
              (loop until ready do (sleep 0.1))
              thread))))

(defgeneric bed (job &key &allow-other-keys)
  (:documentation "Run the BED evolutionary loop for JOB.")
  (:method :around ((job bed-job) &key
                    (population nil)
                    (population-size 64)
                    (max-generations infinity)
                    (test-script nil)
                    ((:test-case-timeout *process-kill-timeout*)
                     +default-test-case-timeout+)
                    ((:mutations *tree-sitter-mutation-types*)
                     +mutations+)
                    &allow-other-keys
                    &aux (gtirb-workspace (gtirb-workspace job))
                      (seeds (population-seeds gtirb-workspace)))
    "Lexically bind variables and perform setup for evolution."
    (with-temporary-script-of (:pathname test-script-path) test-script
      (let* ((*target-fitness-p* #'target-fitness-p)
             (*worst-fitness-p* #'worst-fitness-p)
             (*disassembly-target* (disassembly-target gtirb-workspace))
             (*test-suite* (if test-script
                               (create-behavioral-tests
                                test-script-path
                                :timeout *process-kill-timeout*)
                               (make 'test-suite)))
             (*fitness-test* {fitness-test-lexicase _
                                                    *disassembly-target*
                                                    *test-suite*})
             (*fitness-evals* (current-fitness-evals gtirb-workspace))
             (*max-population-size* (if population
                                        (length population)
                                        population-size))
             (*generations* (floor (/ *fitness-evals* *max-population-size*)))
             (*population* (or population
                               (nest (initial-population)
                                     (compilable-seeds)
                                     (evaluate-seeds seeds))))
             (*tournament-selector* #'lexicase-select)
             #+sbcl
             (max-serial-number (if population
                                    (maximum-serial-number population)
                                    -1)))

        ;; Clear final result if the client requests more evolution generations.
        (when (> max-generations *generations*)
          (nest (clear-bed-results gtirb-workspace)
                (list)
                (format nil "~a-final" +bed-prefix+)))

        ;; Evaluate population fitness and warn of non-compiling seeds.
        (if (null *population*)
            (error "Cannot compile any seed decompilations.")
            (pmap 'list {evaluate *fitness-test*} *population*))
        (and-let* (((gtirb-muse-running-p))
                   ((null population))
                   (non-compiling-seeds (non-compilable-seeds seeds)))
          (warn-non-compilable-seeds gtirb-workspace non-compiling-seeds))

        ;; Reset the serial number counter if population restored from saved
        ;; store file on disk.
        #+sbcl
        (maxf *sbcl-next-serial-number*
              *sbcl-next-serial-number*
              (1+ max-serial-number))

        (call-next-method))))
  (:method ((job bed-job) &key
            (max-generations infinity)
            (max-time infinity)
            (pre-fn (constantly nil))
            (period-fn {checkpoint job})
            (post-fn (constantly nil))
            &allow-other-keys)
    (funcall period-fn)
    (evolve-loop :test *fitness-test*
                 :max-time max-time
                 :max-generations max-generations
                 :pre-fn pre-fn
                 :period-fn period-fn
                 :post-fn post-fn)
    (funcall period-fn "final"))
  (:method :after ((job bed-job) &key &allow-other-keys)
    "Perform post-processing after job completion."
    (setf (slot-value job 'job-status) :job-finished)
    (when (gtirb-muse-running-p)
      (show-message "Decompilation process finished."))))

(-> evolve-loop (&key (:test function)
                   (:max-time number)
                   (:max-generations number)
                   (:pre-fn function)
                   (:post-fn function)
                   (:period-fn function)
                   (:filter function))
                (values t &optional))
(defun evolve-loop (&key (test *fitness-test*)
                      (max-time infinity)
                      (max-generations infinity)
                      (pre-fn (constantly nil))
                      (post-fn (constantly nil))
                      (period-fn (constantly nil))
                      (filter [#'not #'worst-fitness-p]))
  "Main evolutionary search loop for BED repair efforts."
  (handler-bind
      ((no-mutation-targets
        (lambda (e)
          (declare (ignorable e))
          (note 3 "Failed to find mutation targets, trying another mutation.")
          (invoke-restart 'try-another-mutation)))
       (error (lambda (e)
                (note 3 "Error: ~A~%" e)
                (when (>= *note-level* 3)
                  (print-backtrace :condition e :stream *note-out*)
                  (format *note-out* "~%~%"))
                (cond
                 ((find-restart 'ignore-failed-mutation)
                  (invoke-restart 'ignore-failed-mutation))
                 ((find-restart 'try-another-mutation)
                  (invoke-restart 'try-another-mutation))
                 (t (error e))))))
    (note 2 "Calling evolve")
    (note 3 "Population Size: ~d~%" (length *population*))
    (note 3 "Random seed: ~a~%" *random-state*)
    (generational-evolve #'simple-reproduce
                         {parallel-evaluate test}
                         #'lexicase-select
                         :max-time max-time
                         :max-generations max-generations
                         :period 1
                         :every-pre-fn pre-fn
                         :every-post-fn post-fn
                         :period-fn period-fn
                         :filter filter)))

(-> bed-candidate-names (gtirb-workspace) (values list &optional))
(defun bed-candidate-names (gtirb-workspace)
  "Return all of the BED candidate decompilation names in
GTIRB-WORKSPACE."
  (nest (remove-if-not {string^= +bed-prefix+})
        (hash-table-keys)
        (candidate-decompilations gtirb-workspace)))

(-> current-fitness-evals (gtirb-workspace) (values fixnum &optional))
(defun current-fitness-evals (gtirb-workspace)
  "Return the number of fitness evaluations at the most recent checkpoint
of the evolutionary process in GTIRB-WORKSPACE."
  (or (extremum (nest (mapcar [#'parse-integer #'lastcar {split-sequence #\-}])
                      (remove-if {string$= "final"})
                      (bed-candidate-names gtirb-workspace))
                #'>)
      0))

(-> population-seeds (gtirb-workspace) (values list &optional))
(defun population-seeds (gtirb-workspace)
  "Return the candidate decompilation seeds in GTIRB-WORKSPACE."
  (let ((bed-candidates (bed-candidate-names gtirb-workspace)))
    (nest (mapcar #'cdr)
          (remove-if [{member _ bed-candidates :test #'equal} #'car])
          (hash-table-alist)
          (candidate-decompilations gtirb-workspace))))

(-> evaluate-seeds (list &optional function) (values list &optional))
(defun evaluate-seeds (seeds &optional (test *fitness-test*))
  "Force the evaluation of the fitness of all SEEDS using TEST."
  (mapcar (lambda (seed)
            (synchronized (seed)
              (setf (fitness seed) (funcall test seed)))
            seed)
          seeds))

(-> compilable-seeds (list) (values list &optional))
(defun compilable-seeds (seeds)
  "Return the compiling candidate decompilation SEEDS."
  (assert (every #'fitness seeds) ()
          "Fitness must be pre-computed for all SEEDS.")
  (remove-if {funcall *worst-fitness-p*} seeds))

(-> non-compilable-seeds (list) (values list &optional))
(defun non-compilable-seeds (seeds)
  "Return the non-compiling candidate decompilation SEEDS."
  (assert (every #'fitness seeds) ()
          "Fitness must be pre-computed for all SEEDS.")
  (set-difference seeds (compilable-seeds seeds)))

(-> warn-non-compilable-seeds (gtirb-workspace list) (values null &optional))
(defun warn-non-compilable-seeds (gtirb-workspace seeds)
  "Show a warning message with the non-compiling SEEDS elided from the initial
population of the evolutionary process in GTIRB-WORKSPACE."
  (when-let* ((fnames (nest (mapcar [#'file-namestring
                                     {candidate-path-from-gtirb-path
                                      (gtirb-path gtirb-workspace)}
                                     #'car])
                            (remove-if-not [{member _ seeds} #'cdr])
                            (hash-table-alist)
                            (candidate-decompilations gtirb-workspace))))
    (show-message (fmt "Cannot compile ~{~a~^, ~} - ~
                        elided from initial population."
                       fnames)
                  :type :warning)))

(-> initial-population (list &optional number) list)
(defun initial-population (seeds &optional (pop-size *max-population-size*))
  "Return an initial population of POP-SIZE members from candidate
decompilation SEEDS."
  (when seeds
    (iter (for i below pop-size)
          (collect (if (< (random 1.0) *crossover-seed-chance*)
                       (crossover-seed seeds)
                       (copy (random-elt seeds)))))))

(-> crossover-seed (list) (values c &optional))
(defun crossover-seed (seeds)
  "Return an initial seed derived from crossing over members of SEEDS to use
in the initial population."
  (let ((a (random-elt seeds)))
    (if (length= 1 seeds)
        (copy a)
        (nest (crossover a)
              (random-elt)
              (remove a seeds)))))

(-> maximum-serial-number (&optional list) (values number &optional))
(defun maximum-serial-number (&optional (population *population*))
  "Return the highest serial number of the ASTs in population."
  (extremum (mapcar #'serial-number (mappend #'asts population)) #'>))

(-> best-poulation-member (&optional list) c)
(defun best-population-member (&optional (population *population*))
  "Return the member of POPULATION with the best fitness."
  (extremum population #'fitness-predicate :key [{coerce _ 'list} #'fitness]))

(-> process-messages (bed-job) t)
(defun process-messages (job)
  "Function to process messages in JOB's message queue and dispatch
to the appropriate handler."
  (iter (for message = (pop-message-queue job))
        (while message)
        (cond ((eq (car message) :inject)
               (apply #'handle-inject-message (cdr message))))))

(-> handle-inject-message (c number) t)
(defun handle-inject-message (candidate num-copies)
  "Handle a message to inject NUM-COPIES of CANDIDATE into the
population."
  (assert (<= num-copies (length *population*)) ()
          "Number of copies to insert must be less than or equal to ~
           the current size of the population.")
  (evaluate *fitness-test* candidate)
  (if (funcall *worst-fitness-p* candidate)
      (show-message "Cannot inject non-compiling candidate into population."
                    :type :error)
      (setf *population*
            (nest (shuffle)
                  (append (repeatedly num-copies (copy candidate)))
                  (drop num-copies)
                  (sort (copy-seq *population*)
                        (complement *fitness-predicate*)
                        :key [{funcall *fitness-scalar-fn*} #'fitness])))))

(-> update-bed-job (bed-job) t)
(defun update-bed-job (job)
  "Function to update the BED job after a new individual is
created and evaluated in the evolutionary loop."
  (with-slots (population best-candidate fitness-evals) job
    (setf population *population*
          best-candidate (best-population-member *population*)
          fitness-evals *fitness-evals*)))

(-> save-job (bed-job &optional (or string number))
             (values null &optional))
(defun save-job (job &optional (name-suffix *fitness-evals*))
  "Function to insert the best candidate decompilation into the
GTIRB file and write the best candidate, updated GTIRB, and evolutionary
population to disk."
  (let* ((best (best-population-member *population*))
         (minimized (minimize-software best))
         (candidate-name (fmt "~a-~a" +bed-prefix+ name-suffix))
         (workspace (gtirb-workspace job)))
    (nest (upsert-candidate-decompilation workspace candidate-name)
          (funcall {copy _ :fitness (fitness best)})
          (clang-format (if (phenome-p minimized) minimized best)
                        "\"{BasedOnStyle: llvm, IndentWidth: 4}\""))
    (when (gtirb-muse-running-p)
      (write-candidate-to-disk workspace candidate-name)
      (write-gtirb-to-disk workspace)
      (write-population-to-disk workspace))))

(-> checkpoint (bed-job &optional (or string number))
               (values null &optional))
(defun checkpoint (job &optional (name-suffix *fitness-evals*))
  "Checkpoint function to perform processing after each generation of the
evolutionary loop."
  (process-messages job)
  (update-bed-job job)
  (save-job job name-suffix))

(-> population-path-from-gtirb-path (pathname) pathname)
(defun population-path-from-gtirb-path (gtirb-path)
  "Return the path to the population store from the given GTIRB-PATH."
  (make-pathname
   :defaults (cache-dir-from-gtirb-path gtirb-path)
   :name "population"
   :type "store"))

(-> write-population-to-disk (gtirb-workspace &optional list)
                             (values null &optional))
(defun write-population-to-disk (gtirb-workspace
                                 &optional (population (copy-seq *population*)))
  "Write the evolutionary POPULATION to disk as a store file in
GTIRB-WORKSPACE."
  (labels ((population-to-base64-string (population)
             "Serialize POPULATION to a base64 string."
             (let ((out (make-in-memory-output-stream)))
               (store population out)
               (usb8-array-to-base64-string (get-output-stream-sequence out)))))

    (call-async *server*
                +gtirbDecompilation/file/write+
                (make 'FileWriteParams
                      :uri (nest (file-path-uri)
                                 (population-path-from-gtirb-path)
                                 (gtirb-path gtirb-workspace))
                      :content (population-to-base64-string population)
                      :encoding :|base64|))))

(-> erase-population-from-disk (gtirb-workspace)
                               (values null &optional))
(defun erase-population-from-disk (gtirb-workspace)
  "Erase the population store file for GTIRB-WORKSPACE from the disk."
  (call-async *server*
              "workspace/applyEdit"
              (nest (make '|ApplyWorkspaceEditParams| :edit)
                    (make '|WorkspaceEdit| :document-changes)
                    (list)
                    (make '|DeleteFile|
                          :uri (nest (file-path-uri)
                                     (population-path-from-gtirb-path)
                                     (gtirb-path gtirb-workspace))
                          :options (make '|DeleteFileOptions|
                                         :ignoreIfNotExists t)))))

(-> job-state-message (bed-job) string)
(defun job-state-message (job)
  "Return a string to be used in a status message representing the
current state of JOB."
  (ecase (job-status job)
    (:job-terminated "terminated")
    (:job-finished "finished")
    (:job-running "running")))

(-> fitness-status-message (bed-job) string)
(defun fitness-status-message (job)
  "Return a string to be used in a status message representing the
current fitness of the best candidate in JOB's population."
  (let* ((*disassembly-target* (disassembly-target (gtirb-workspace job)))
         (candidate (best-candidate job))
         (disassembly-fitness (disassembly-fitness candidate))
         (behavioral-fitness (behavioral-fitness candidate)))
    (nest (apply #'concatenate 'string)
          (list (fmt "~,2f% byte-equivalent"
                     (* 100 (mean disassembly-fitness)))
                (if (length> behavioral-fitness 0)
                    (fmt ", ~d/~d tests passing"
                         (count-if-not #'zerop behavioral-fitness)
                         (length behavioral-fitness))
                    "")))))

(defgeneric show-bed-job-status (job)
  (:documentation "Show a message with the status of JOB to the client.")
  (:method ((job bed-job))
    (cond ((and (null (best-candidate job))
                (not (eq (job-status job) :job-running)))
           (show-message "Decompilation terminated."))
          ((null (best-candidate job))
           (show-message "Creating initial evolutionary population."))
          (t (show-message (fmt "Decompilation ~a: ~a, ~d evals."
                                (job-state-message job)
                                (fitness-status-message job)
                                (fitness-evals job)))))))

(defgeneric stop-bed-job (job)
  (:documentation "Terminate the given JOB.")
  (:method ((job bed-job))
    (with-slots (worker-thread job-status) job
      (setf job-status :job-terminated)
      (when (and worker-thread (thread-alive-p worker-thread))
        (ignore-errors (destroy-thread worker-thread))))))

(defgeneric clear-bed-results (workspace &optional bed-candidates)
  (:documentation "Remove BED-CANDIDATES from the GTIRB WORKSPACE.")
  (:method ((gtirb-workspace gtirb-workspace) &optional
            (bed-candidates (bed-candidate-names gtirb-workspace)))
    (assert (null (remove-if {string^= +bed-prefix+} bed-candidates))
            (bed-candidates)
            "Cannot clear non-BED results.")
    (remove-candidate-decompilations gtirb-workspace bed-candidates)
    (when (gtirb-muse-running-p)
      (erase-candidates-from-disk gtirb-workspace bed-candidates))))
