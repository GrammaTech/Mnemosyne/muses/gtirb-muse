;;; AST utilities
(defpackage :gtirb-muse/ast-utilities
  (:use :gt/full
        :functional-trees/attrs
        :functional-trees/sel-attributes
        :software-evolution-library/software/parseable
        :software-evolution-library/software/c
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/readtable)
  (:import-from :fset :empty-set :set)
  (:export :copy-ast-only
           :top-level-functions
           :function-declarations
           :function-declarators
           :type-declarations
           :global-declarations
           :headers
           :function-dependencies
           :function-declaration-dependencies
           :type-declaration-dependencies
           :type-declarations-utilized
           :global-declarations-utilized
           :function-callsites
           :parameter-identifiers
           :unused-parameters
           :functions-with-unused-parameter
           :parameter-arity-mismatch-p
           :parameter-arity-<
           :parameter-arity->
           :parameter-arity-mismatches
           :variadic-parameters-p
           :void-function-p
           :callsite-function
           :name
           :name-equal
           :declaration-names
           :declaration-names-intersection))
(in-package :gtirb-muse/ast-utilities)
(in-readtable gtirb-muse-readtable)

(-> copy-ast-only (ast &key (:before-text string)
                            (:after-text string)
                            (:before-asts list)
                            (:after-asts list))
                  (values ast &optional))
(defun copy-ast-only (ast &key (before-text "")
                               (after-text "")
                               (before-asts nil)
                               (after-asts nil))
  "Copy AST with no surrounding text or ASTs."
  (copy ast
        :before-text before-text
        :after-text after-text
        :before-asts before-asts
        :after-asts after-asts))

(-> top-level-functions (root-ast) (values list &optional))
(defun top-level-functions (root)
  "Return all of the top-level functions in ROOT."
  (nest (remove-if-not (of-type 'function-ast))
        (children root)))

(-> function-declarations (root-ast) (values list &optional))
(defun function-declarations (root)
  "Return all of the top-level function declarations in ROOT."
  (nest (remove-if-not #'function-declaration-p)
        (remove-if-not (of-type 'c-declaration))
        (children root)))

(-> function-declarators (root-ast) (values list &optional))
(defun function-declarators (root)
  "Return all of the top-level function declarators in ROOT."
  (mapcar [#'first #'c-declarator] (function-declarations root)))

(-> type-declarations (root-ast) (values list &optional))
(defun type-declarations (root)
  "Return all of the top-level type declarations in ROOT."
  (nest (remove-if-not (of-type 'type-declaration-ast))
        (children root)))

(-> global-declarations (root-ast) (values list &optional))
(defun global-declarations (root)
  "Return all of the top-level global declarations in ROOT."
  (nest (remove-if #'function-declaration-p)
        (remove-if-not (of-type 'c-declaration))
        (children root)))

(-> headers (root-ast) (values list &optional))
(defun headers (root)
  "Return all of the top-level include directives in ROOT."
  (nest (remove-if-not (of-type 'c-preproc-include))
        (children root)))

(-> function-declaration-p (c-declaration) boolean)
(defun function-declaration-p (ast)
  "Return TRUE if AST is a function declaration."
  (typep (first (c-declarator ast)) 'c-function-declarator))

(-> function-dependencies (function-ast list) list)
(defun function-dependencies (function-ast functions
                              &aux (visited (empty-set)))
  "Return FUNCTION-AST and its dependent functions in FUNCTION-ASTS."
  (labels ((helper (function-ast functions)
             (unless (contains? visited function-ast)
               (setf visited (with visited function-ast))
               (nest (cons function-ast)
                     (mappend {helper _ functions})
                     (remove-if #'null)
                     (mapcar {find _ functions :test #'name-equal})
                     (convert 'list)
                     (uses)
                     (function-body function-ast)))))
    (helper function-ast functions)))

(-> function-declaration-dependencies (list list) (values list &optional))
(defun function-declaration-dependencies (functions function-declarations)
  "Return the function declarations for the given list of FUNCTIONS in
ROOT."
  (nest (remove-if #'null)
        (mapcar {find _ function-declarations
                        :key [#'first #'c-declarator]
                        :test #'name-equal}
                functions)))

(-> type-declaration-dependencies (type-declaration-ast list) list)
(defun type-declaration-dependencies (type-declaration type-declarations
                                      &aux (visited (empty-set)))
  "Return TYPE-DECLARATION and its dependent types in TYPE-DECLARATIONS."
  ;; TODO: Is it possible to use `get-declaration-ast` here?
  (labels ((helper (td tds)
             (unless (contains? visited td)
               (setf visited (with visited td))
               (nest (cons td)
                     (mappend {helper _ tds})
                     (remove-if «or #'null {name-equal td}»)
                     (mapcar {find _ tds :test #'name-equal})
                     (remove-if (of-type 'c-primitive-type))
                     (convert 'list)
                     (typenames td)))))
    (helper type-declaration type-declarations)))

(-> type-declarations-utilized (list list list) list)
(defun type-declarations-utilized (functions globals type-declarations)
  "Return the type declarations in TYPE-DECLARATIONS FUNCTIONS and GLOBALS
utilize."
  (nest (remove-duplicates)
        (mappend {type-declaration-dependencies _ type-declarations})
        (remove-if #'null)
        (mapcar {find _ type-declarations :test #'name-equal})
        (convert 'list)
        (reduce #'union (append functions globals)
                :key #'typenames
                :initial-value (empty-set))))

(-> global-declarations-utilized (list list) list)
(defun global-declarations-utilized (functions globals)
  "Return the global variables in GLOBALS FUNCTIONS utilize."
  (nest (remove-duplicates)
        (remove-if #'null)
        (mapcar {find _ globals :test #'name-in-declaration-p})
        (convert 'list)
        (reduce #'union functions
                :key #'uses
                :initial-value (empty-set))))

(-> function-callsites (root-ast function-ast) (values list &optional))
(defun function-callsites (root function)
  "Return the callsites of FUNCTION in ROOT."
  ;; NOTE: There may be issues in the presence of shadowing; however, the
  ;; decompilation seeds used in this context do not utilize shadowing.
  (nest (remove-if-not {name-equal function})
        (remove-if-not (of-type 'call-ast))
        (convert 'list root)))

(-> parameter-identifiers (function-ast) (values list &optional))
(defun parameter-identifiers (function)
  "Return a list of IDENTIFIER-ASTs for the parameters of FUNCTION."
  (nest (mapcar (lambda (names) (if (length= 1 names) (only-elt names) nil)))
        (mapcar #'parameter-names)
        (function-parameters function)))

(-> unused-parameters (function-ast) (values list &optional))
(defun unused-parameters (function)
  "Return a list of IDENTIFIER-ASTs representing the parameters of FUNCTION
which are not utilized in the FUNCTION body."
  (set-difference (nest (remove-if #'null)
                        (parameter-identifiers function))
                  (nest (identifiers)
                        (function-body function))
                  :test #'name-equal))

(-> functions-with-unused-parameter (root-ast) (values list &optional))
(defun functions-with-unused-parameter (root)
  "Return a list of FUNCTION-ASTs in ROOT with one or more unused
parameters."
  (nest (remove-if-not #'unused-parameters)
        (top-level-functions root)))

(-> parameter-arity-mismatch-p (function-ast function-ast)
                               (values boolean &optional))
(defun parameter-arity-mismatch-p (function-a function-b)
  "Return non-NIL if FUNCTION-A and FUNCTION-B contain a different number
of parameters."
  (or (parameter-arity-< function-a function-b)
      (parameter-arity-> function-a function-b)))

(-> parameter-arity-< (function-ast function-ast)
                      (values boolean &optional))
(defun parameter-arity-< (function-a function-b)
  "Return non-NIL if FUNCTION-A has strictly less parameters than FUNCTION-B."
  (length< (function-parameters function-a)
           (function-parameters function-b)))

(-> parameter-arity-> (function-ast function-ast)
                      (values boolean &optional))
(defun parameter-arity-> (function-a function-b)
  "Return non-NIL if FUNCTION-A has strictly more parameters than FUNCTION-B."
  (parameter-arity-< function-b function-a))

(-> parameter-arity-mismatches (list list) (values list &optional))
(defun parameter-arity-mismatches (functions-a functions-b)
  "Return a list of pairs of function ASTs in FUNCTIONS-A and FUNCTIONS-B which
have the same name but a different number of parameters."
  ;; FUNCTIONS-A and FUNCTIONS-B should be small (only one element
  ;; *crossover-recursive-chance* percent of the time).  No performance
  ;; regression has been noted, but if performance appears to suffer,
  ;; we could create a table for FUNCTIONS-B mapping name -> arity
  ;; to avoid calling #'find below.
  (nest (remove-if [#'null #'second])
        (mapcar (lambda (function-a)
                  (list function-a
                        (find function-a functions-b
                              :test «and #'name-equal
                                         #'parameter-arity-mismatch-p»)))
                functions-a)))

(-> variadic-parameters-p (function-ast) (values (or ast null) &optional))
(defun variadic-parameters-p (function)
  "Return non-NIL if FUNCTION utilizes variadic parameters."
  (find-if (of-type 'c-variadic-parameter) (function-parameters function)))

(-> void-function-p (function-ast) boolean)
(defun void-function-p (function)
  "Return non-NIL if FUNCTION's return type is void."
  (equal (source-text (c-type function)) "void"))

(-> callsite-function (call-ast list) (values (or function-ast null) &optional))
(defun callsite-function (callsite functions)
  "Return the function in FUNCTIONS corresponding to the function call
at CALLSITE, if possible."
  (find callsite functions :test #'name-equal))

(def-attr-fun types ()
  "Return the types directly used (but not declared)."
  (:method ((ast c-primitive-type))
    (set ast))
  (:method ((ast c-type-identifier))
    (set ast))
  (:method ((ast c-enum-specifier))
    (set ast))
  (:method ((ast c-type-definition))
    (types (c-type ast)))
  (:method ((ast composite-type-ast))
    (union (set ast)
           (if (c/cpp-body ast)
               (types (c/cpp-body ast))
               (empty-set))))
  (:method ((ast ast))
    (reduce #'union (children ast)
            :key #'types
            :initial-value (empty-set))))

(defgeneric name (ast)
  (:documentation "Return the name associated with the given AST, if possible.
For instance, for function definitions, return the function name.")
  (:method ((ast ast))
    (declarator-name ast))
  (:method ((ast function-ast))
    (definition-name ast))
  (:method ((ast call-ast))
    (source-text (call-function ast)))
  (:method ((ast c-macro-forward-declaration))
    (name (first (children ast))))
  (:method ((ast c-type-forward-declaration))
    (name (first (children ast))))
  (:method ((ast c-enum-specifier))
    (fmt "enum ~a" (name (c-name ast))))
  (:method ((ast c-union-specifier))
    (fmt "union ~a" (name (c-name ast))))
  (:method ((ast c-struct-specifier))
    (fmt "struct ~a" (name (c-name ast))))
  (:method ((ast c-type-definition))
    (name (first (c-declarator ast))))
  (:method ((ast c-preproc-include))
    (let ((text (source-text (c-path ast))))
      (subseq text 1 (1- (length text)))))
  (:method ((ast c-primitive-type))
    (source-text ast))
  (:method ((ast c-type-identifier))
    (source-text ast)))

(defgeneric name-equal (a b)
  (:documentation "Return TRUE if the `name` method returns the same value
for a and b.")
  (:method ((a t) (b t))
    (equal a b))
  (:method ((a ast) (b t))
    (name-equal (name a) b))
  (:method ((a t) (b ast))
    (name-equal a (name b)))
  (:method ((a ast) (b ast))
    (name-equal (name a) (name b))))

(-> typenames (ast) (values set &optional))
(defun typenames (ast)
  "Return the names of the types directly used by AST."
  (image #'name (types ast)))

(-> declaration-names (c-declaration) (values list &optional))
(defun declaration-names (decl)
  "Return the names declared by the DECL AST."
  (mapcar #'name (c-declarator decl)))

(-> name-in-declaration-p (string c-declaration)
                          (values (or string null) &optional))
(defun name-in-declaration-p (name decl)
  "Return non-NIL if NAME is declared in DECL."
  (find name (declaration-names decl) :test #'name-equal))

(-> declaration-names-intersection (c-declaration c-declaration)
                                   (values list &optional))
(defun declaration-names-intersection (a b)
  "Return the list of names declared by both A and B."
  (intersection (declaration-names a) (declaration-names b) :test #'name-equal))
