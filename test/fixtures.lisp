(defpackage :gtirb-muse/test/fixtures
  (:use :gt/full
        :gtirb
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/readtable
        :gtirb-muse/gtirb-workspace
        :gtirb-muse/test/utilities)
  (:shadowing-import-from :gtirb :size :symbol)
  (:export :hello-world
           :simple-crossover
           :parameter-mismatch-crossover
           :parameter-mismatch-unused-crossover
           :*hello-world*
           :*hello-world-gtirb-path*
           :*hello-world-gtirb*
           :*hello-world-gtirb-workspace*
           :*hello-world-test-script*
           :*crossover-1*
           :*crossover-2*))
(in-package :gtirb-muse/test/fixtures)
(in-readtable gtirb-muse-readtable)


;;; Common fixtures and variables.
(defparameter *hello-world* nil)
(defparameter *hello-world-gtirb-path* nil)
(defparameter *hello-world-gtirb* nil)
(defparameter *hello-world-gtirb-workspace* nil)
(defparameter *hello-world-test-script* nil)
(defparameter *crossover-1* nil)
(defparameter *crossover-2* nil)

(defixture hello-world
  (:setup
    (setf *hello-world*
          (from-file (make 'c)
                     (path-join +etc-dir+ "hello/hello.c"))
          *hello-world-gtirb-path*
          (path-join +etc-dir+ "hello/hello.gtirb")
          *hello-world-gtirb*
          (read-gtirb *hello-world-gtirb-path* )
          *hello-world-gtirb-workspace*
          (make 'gtirb-workspace
                :gtirb-path *hello-world-gtirb-path*
                :gtirb *hello-world-gtirb*)
          *hello-world-test-script*
          (path-join +etc-dir+ "hello/test.sh")))
  (:teardown
    (setf *hello-world* nil
          *hello-world-gtirb-path* nil
          *hello-world-gtirb* nil
          *hello-world-gtirb-workspace* nil
          *hello-world-test-script* nil)))

(defixture simple-crossover
  (:setup
    (setf *crossover-1*
          (nest (from-file (make 'c))
                (path-join +etc-dir+ "crossover/simple/"
                           "crossover-1.c"))
          *crossover-2*
          (nest (from-file (make 'c))
                (path-join +etc-dir+ "crossover/simple/"
                           "crossover-2.c"))))
  (:teardown
    (setf *crossover-1* nil
          *crossover-2* nil)))

(defixture parameter-mismatch-crossover
  (:setup
    (setf *crossover-1*
          (nest (from-file (make 'c))
                (path-join +etc-dir+ "crossover/mismatched-parameters/"
                           "crossover-1.c"))
          *crossover-2*
          (nest (from-file (make 'c))
                (path-join +etc-dir+ "crossover/mismatched-parameters/"
                           "crossover-2.c"))))
  (:teardown
    (setf *crossover-1* nil
          *crossover-2* nil)))

(defixture parameter-mismatch-unused-crossover
  (:setup
    (setf *crossover-1*
          (nest (from-file (make 'c))
                (path-join +etc-dir+ "crossover/mismatched-parameters-unused/"
                           "crossover-1.c"))
          *crossover-2*
          (nest (from-file (make 'c))
                (path-join +etc-dir+ "crossover/mismatched-parameters-unused/"
                           "crossover-2.c"))))
  (:teardown
    (setf *crossover-1* nil
          *crossover-2* nil)))
