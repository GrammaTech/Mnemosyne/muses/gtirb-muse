
#include <stdio.h>

int main(void);
void function2();

struct my_struct {
    int field;
};
typedef struct my_struct my_struct;

my_struct global2 = {0};

int main(void) {
    function2();
    printf("%d\n", global2.field);
}

void function2() {
    global2.field = 2;
}
