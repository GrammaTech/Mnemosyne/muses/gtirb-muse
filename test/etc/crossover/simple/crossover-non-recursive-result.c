
#include <stdio.h>


int main(void);
void function1();

typedef int int32;

struct my_struct {
    int field;
};
typedef struct my_struct my_struct;

int32 global1 = 0;

my_struct global2 = {0};

int main(void) {
    function2();
    printf("%d\n", global2.field);
}

void function1() {
    global1 = 1;
}
