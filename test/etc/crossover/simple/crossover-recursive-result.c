
#include <stdio.h>


int main(void);
void function1();
void function2();

typedef int int32;

struct my_struct {
    int field;
};
typedef struct my_struct my_struct;

int32 global1 = 0;

my_struct global2 = {0};

void function2() {
    global2.field = 2;
}


int main(void) {
    function2();
    printf("%d\n", global2.field);
}

void function1() {
    global1 = 1;
}
