#include <stdlib.h>
#include <stdio.h>

void hello(char* message){
	printf(message);
}

int main()
{
   //Calling a function here
   char message[100];
   sprintf(message, "%s", "Hello World!");
   hello(message);
   return 0;
}
