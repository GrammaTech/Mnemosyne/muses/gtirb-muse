#!/bin/bash

case "$1" in
    num-tests)
        echo 2;;
    test)
        case $3 in
            0) $2;;
            1) $2 | grep "Hello World!";;
        esac;;
esac
