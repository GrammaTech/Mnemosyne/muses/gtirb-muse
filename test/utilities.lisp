(defpackage :gtirb-muse/test/utilities
  (:use :gt/full
        :stefil+)
  (:export :test
           :+etc-dir+))
(in-package :gtirb-muse/test/utilities)

(defroot test)

(define-constant +etc-dir+
  (asdf:system-relative-pathname :gtirb-muse "test/etc/")
  :test #'equalp
  :documentation "Directory containing gtirb muse unit test files.")
