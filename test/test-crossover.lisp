(defpackage :gtirb-muse/test/test-crossover
  (:use :gt/full
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/ast-utilities
        :gtirb-muse/crossover
        :gtirb-muse/readtable
        :gtirb-muse/test/fixtures
        :gtirb-muse/test/utilities))
(in-package :gtirb-muse/test/test-crossover)
(in-readtable gtirb-muse-readtable)

(defsuite test-crossover "Tests for crossover.")

(-> function-1-selector (list) (values function-ast &optional))
(defun function-1-selector (pool)
  (find-if {name-equal "function1"} pool))

(deftest (test-simple-crossover-non-recursive :long-running) ()
  (with-fixture simple-crossover
    (let ((*crossover-recursive-chance* 0.0))
      (is (string= (nest (source-text)
                         (crossover *crossover-1* *crossover-2*))
                   (nest (read-file-into-string)
                         (path-join +etc-dir+ "crossover/simple/"
                                    "crossover-non-recursive-result.c")))))))

(deftest (test-simple-crossover-recursive :long-running) ()
  (with-fixture simple-crossover
    (let ((*crossover-recursive-chance* 1.0))
      (is (string= (nest (source-text)
                         (crossover *crossover-1* *crossover-2*))
                   (nest (read-file-into-string)
                         (path-join +etc-dir+ "crossover/simple/"
                                    "crossover-recursive-result.c")))))))

(deftest (test-parameter-mismatch-fewer-crossover :long-running) ()
  (with-fixture parameter-mismatch-crossover
    (let ((*crossover-recursive-chance* 0.0)
          (*crossover-selection-function* #'function-1-selector))
      (is (string= (nest (source-text)
                         (crossover *crossover-1* *crossover-2*))
                   (nest (read-file-into-string)
                         (path-join +etc-dir+
                                    "crossover/mismatched-parameters/"
                                    "crossover-fewer-result.c")))))))

(deftest (test-parameter-mismatch-greater-crossover :long-running) ()
  (with-fixture parameter-mismatch-crossover
    (let ((*crossover-recursive-chance* 0.0)
          (*crossover-selection-function* #'function-1-selector))
      (is (string= (nest (source-text)
                         (crossover *crossover-2* *crossover-1*))
                   (nest (read-file-into-string)
                         (path-join +etc-dir+
                                    "crossover/mismatched-parameters/"
                                    "crossover-greater-result.c")))))))

(deftest (test-parameter-mismatch-unused-crossover :long-running) ()
  (with-fixture parameter-mismatch-unused-crossover
    (let ((*crossover-recursive-chance* 0.0)
          (*crossover-selection-function* #'function-1-selector))
      (is (string= (nest (source-text)
                         (crossover *crossover-1* *crossover-2*))
                   (nest (read-file-into-string)
                         (path-join +etc-dir+
                                    "crossover/mismatched-parameters-unused/"
                                    "crossover-result.c")))))))
