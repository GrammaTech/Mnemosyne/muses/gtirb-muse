(defpackage :gtirb-muse/test/test-path-utilities
  (:use :lsp-server/protocol
        :gt/full
        :stefil+
        :gtirb-muse/path-utilities
        :gtirb-muse/readtable
        :gtirb-muse/test/utilities))
(in-package :gtirb-muse/test/test-path-utilities)
(in-readtable gtirb-muse-readtable)

(defsuite test-path-utilities "Path utilities")

(deftest test-asm-view-path-from-params ()
  (is (null (nest (asm-view-path-from-params)
                  (make 'DidOpenTextDocumentParams :text-document)
                  (make 'TextDocumentItem
                        :uri "file:///tmp/t.view"
                        :language-id ""
                        :version 0
                        :text ""))))
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/x64/hello.view")
             (nest (asm-view-path-from-params)
                   (make 'DidOpenTextDocumentParams :text-document)
                   (make 'TextDocumentItem
                         :uri "file:///tmp/.vscode.hello.gtirb/x64/hello.view"
                         :language-id ""
                         :version 0
                         :text "")))))

(deftest test-candidate-path-from-params ()
  (is (null (nest (candidate-path-from-params)
                  (make 'DidOpenTextDocumentParams :text-document)
                  (make 'TextDocumentItem
                        :uri "file:///tmp/t.c"
                        :language-id ""
                        :version 0
                        :text ""))))
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/hello_retdec.c")
             (nest (candidate-path-from-params)
                   (make 'DidOpenTextDocumentParams :text-document)
                   (make 'TextDocumentItem
                         :uri "file:///tmp/.vscode.hello.gtirb/hello_retdec.c"
                         :language-id ""
                         :version 0
                         :text "")))))

(deftest test-gtirb-path-from-file-path ()
  (is (null (gtirb-path-from-file-path (pathname "/tmp/t.c"))))
  (is (equal (pathname "/tmp/hello.gtirb")
             (nest (gtirb-path-from-file-path)
                   (pathname "/tmp/.vscode.hello.gtirb/hello_retdec.c"))))
  (is (equal (pathname "/tmp/hello.gtirb")
             (nest (gtirb-path-from-file-path)
                   (pathname "/tmp/.vscode.hello.gtirb/x64/hello.view")))))

(deftest test-asm-view-path-p ()
  (is (not (asm-view-path-p (pathname "/tmp/t.view"))))
  (is (asm-view-path-p (pathname "/tmp/.vscode.hello.gtirb/x64/hello.view"))))

(deftest test-candidate-path-p ()
  (is (not (candidate-path-p (pathname "/tmp/t.c"))))
  (is (candidate-path-p (pathname "/tmp/.vscode.hello.gtirb/hello_retdec.c"))))

(deftest test-cache-dir-p ()
  (is (not (cache-dir-p (pathname "/tmp/"))))
  (is (not (cache-dir-p (pathname "/tmp/tmp2/"))))
  (is (cache-dir-p (pathname "/tmp/.vscode.hello.gtirb/"))))

(deftest test-cache-dir-from-asm-view-path ()
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/")
             (nest (cache-dir-from-asm-view-path)
                   (pathname "/tmp/.vscode.hello.gtirb/x64/hello.view")))))

(deftest test-cache-dir-from-candidate-path ()
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/")
             (nest (cache-dir-from-candidate-path)
                   (pathname "/tmp/.vscode.hello.gtirb/hello_retdec.c")))))

(deftest test-cache-dir-from-gtirb-path ()
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/")
             (nest (cache-dir-from-gtirb-path)
                   (pathname "/tmp/hello.gtirb")))))

(deftest test-candidate-path-from-gtirb-path ()
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/hello_retdec.c")
             (candidate-path-from-gtirb-path (pathname "/tmp/hello.gtirb")
                                             "Retdec")))
  (is (equal (pathname "/tmp/.vscode.hello.gtirb/my-candidate.c")
             (candidate-path-from-gtirb-path (pathname "/tmp/hello.gtirb")
                                             "my-candidate"))))

(deftest test-candidate-name-from-path ()
  (is (nest (equal "Retdec")
            (candidate-name-from-path)
            (pathname "/tmp/.vscode.hello.gtirb/hello_retdec.c")))
  (is (nest (equal "bed-0")
            (candidate-name-from-path)
            (pathname "/tmp/.vscode.hello.gtirb/hello_bed-0.c")))
  (is (nest (equal "bed-final")
            (candidate-name-from-path)
            (pathname "/tmp/.vscode.hello.gtirb/hello_bed-final.c")))
  (is (nest (equal "my-candidate")
            (candidate-name-from-path)
            (pathname "/tmp/.vscode.hello.gtirb/my-candidate.c")))
  (is (nest (equal "MY_CANDIDATE")
            (candidate-name-from-path)
            (pathname "/tmp/.vscode.hello.gtirb/MY_CANDIDATE.c"))))

(deftest test-client-candidate-name-p ()
  (is (not (client-candidate-name-p "Retdec")))
  (is (not (client-candidate-name-p "RETDEC")))
  (is (not (client-candidate-name-p "retdec")))
  (is (not (client-candidate-name-p "Ghidra")))
  (is (not (client-candidate-name-p "GHIDRA")))
  (is (not (client-candidate-name-p "ghidra")))
  (is (not (client-candidate-name-p "Dwarf")))
  (is (not (client-candidate-name-p "DWARF")))
  (is (not (client-candidate-name-p "dwarf")))
  (is (not (client-candidate-name-p "Bed-0")))
  (is (not (client-candidate-name-p "bed-0")))
  (is (not (client-candidate-name-p "bed-0")))
  (is (client-candidate-name-p "my-candidate"))
  (is (client-candidate-name-p "MY_CANDIDATE")))
