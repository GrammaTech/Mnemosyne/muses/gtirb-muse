(defpackage :gtirb-muse/test/test-bed
  (:use :gt/full
        :lparallel
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/bed
        :gtirb-muse/fitness
        :gtirb-muse/gtirb-workspace
        :gtirb-muse/readtable
        :gtirb-muse/test/fixtures
        :gtirb-muse/test/utilities)
  (:import-from :gtirb :read-gtirb)
  (:import-from :gtirb-muse/bed
                :best-candidate
                :best-population-member
                :population-seeds
                :evaluate-seeds
                :compilable-seeds
                :non-compilable-seeds
                :fitness-evals
                :initial-population
                :population
                :worker-thread))
(in-package :gtirb-muse/test/test-bed)
(in-readtable gtirb-muse-readtable)


;;; Test suite definition.
(defsuite test-bed "Tests for BED jobs."
  (and (which +gtirb-objdump+)
       (which +ddisasm+)))


;;; Helper macros and functions.
(defmacro with-fitness-bindings (&body body)
  `(let* ((*disassembly-target* (disassembly-target *hello-world-gtirb-workspace*))
          (*fitness-test* #'fitness-test-lexicase)
          (*worst-fitness-p* #'worst-fitness-p)
          (*kernel* (make-kernel 1)))
     ,@body))


;;; Unit test implementations.
(deftest (test-compilable-seeds :long-running) ()
  (with-fixture hello-world
    (with-fitness-bindings
      (is (nest (length= 2)
                (compilable-seeds)
                (evaluate-seeds)
                (population-seeds *hello-world-gtirb-workspace*))))))

(deftest (test-non-compilable-seeds :long-running) ()
  (with-fixture hello-world
    (with-fitness-bindings
      (is (nest (length= 0)
                (non-compilable-seeds)
                (evaluate-seeds)
                (population-seeds *hello-world-gtirb-workspace*))))))

(deftest (test-initial-population :long-running) ()
  (with-fixture hello-world
    (with-fitness-bindings
      (let* ((*max-population-size* 64)
             (*crossover-seed-chance* 1/3)
             (seeds (nest (compilable-seeds)
                          (evaluate-seeds)
                          (population-seeds *hello-world-gtirb-workspace*)))
             (population (initial-population seeds)))
        (is (length= *max-population-size* (length population)))
        (destructuring-bind (a-seed b-seed) seeds
          ;; Population should be a mix of the a-seed, b-seed, and
          ;; crossovers of both.
          (is (some [{equalp (genome a-seed)} #'genome] population))
          (is (some [{equalp (genome b-seed)} #'genome] population))
          (is (notevery «or [{equalp (genome a-seed)} #'genome]
                            [{equalp (genome b-seed)} #'genome]»
                        population)))))))

(deftest (test-bed-job-evolve-loop :long-running) ()
  (with-fixture hello-world
    (with-fitness-bindings
      (let ((job (make 'bed-job
                       :gtirb-workspace *hello-world-gtirb-workspace*
                       :population-size 5
                       :max-generations 1
                       :test-script (file-to-string *hello-world-test-script*)
                       :mutations '((tree-sitter-insert  . 1/6)
                                    (tree-sitter-swap    . 1/3)
                                    (tree-sitter-move    . 1/2)
                                    (tree-sitter-replace . 2/3)
                                    (tree-sitter-cut     . 5/6)
                                    (tree-sitter-nop     . 1)))))
        (is (equal (job-status job) :job-finished))
        (is (equal 5 (fitness-evals job)))
        (is (equal 5 (length (population job))))
        (is (equal (best-candidate job)
                   (best-population-member (population job))))))))

(deftest (test-bed-job-inject-candidate :long-running) ()
  (with-fixture hello-world
    (with-fitness-bindings
      (let ((job (make 'bed-job
                       :gtirb-workspace *hello-world-gtirb-workspace*
                       :population-size 5
                       :max-generations 0
                       :test-script (file-to-string *hello-world-test-script*)
                       :threaded t))
            (candidate (from-string (make 'c) "int main() { return 0; }")))
        (push-message-queue job (list :inject candidate 5))
        (join-thread (worker-thread job))
        (is (length= (count (genome-string candidate) (population job)
                            :key #'genome-string
                            :test #'equal)
                     (population job)))))))
