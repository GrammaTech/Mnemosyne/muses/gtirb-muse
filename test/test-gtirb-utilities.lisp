(defpackage :gtirb-muse/test/test-gtirb-utilities
  (:use :gt/full
        :gtirb
        :stefil+
        :gtirb-muse/gtirb-utilities
        :gtirb-muse/readtable
        :gtirb-muse/test/fixtures
        :gtirb-muse/test/utilities)
  (:shadowing-import-from :gtirb :size :symbol))
(in-package :gtirb-muse/test/test-gtirb-utilities)
(in-readtable gtirb-muse-readtable)

(defsuite test-gtirb-utilities "GTIRB utilities")

(defparameter *hello-function-uuid* -74978367421806813279371804371705105975)

(deftest test-gtirb-ir-sources ()
  (with-fixture hello-world
    (is (equalp '("Ghidra" "Retdec")
                (sort (nest (hash-table-keys)
                            (aux-data-data)
                            (gtirb-ir-sources *hello-world-gtirb*))
                      #'string<)))))

(deftest test-gtirb-function-names ()
  (with-fixture hello-world
    (let ((fnames (nest (gtirb-function-names)
                        (first)
                        (modules *hello-world-gtirb*))))
      (is (= 17 (hash-table-count (aux-data-data fnames))))
      (is (equalp "hello"
                  (name (get-uuid (nest (gethash *hello-function-uuid*)
                                        (aux-data-data fnames))
                                  *hello-world-gtirb*)))))))

(deftest test-gtirb-function-blocks ()
  (with-fixture hello-world
    (let ((fblocks (nest (gtirb-function-blocks)
                         (first)
                         (modules *hello-world-gtirb*))))
      (is (= 17 (hash-table-count (aux-data-data fblocks))))
      (is (length= 3 (nest (gethash *hello-function-uuid*)
                           (aux-data-data fblocks)))))))

(deftest test-gtirb-function-sources ()
  (with-fixture hello-world
    (let ((fsources (nest (gtirb-function-sources)
                          (first)
                          (modules *hello-world-gtirb*))))
      (is (= 2 (hash-table-count (aux-data-data fsources))))
      (is (equalp '("Ghidra" "Retdec")
                  (sort (nest (hash-table-keys)
                              (gethash *hello-function-uuid*)
                              (aux-data-data fsources))
                        #'string<))))))

(deftest test-gtirb-function-name-for-uuid ()
  (with-fixture hello-world
    (is (null (gtirb-function-name-for-uuid *hello-world-gtirb* 0)))
    (is (equalp "hello"
                (gtirb-function-name-for-uuid
                 *hello-world-gtirb*
                 *hello-function-uuid*)))))

(deftest test-aux-data-insert ()
  (with-fixture hello-world
    (is (equalp '("Ghidra" "Retdec" "Test")
                (sort (nest (hash-table-keys)
                            (aux-data-upsert "Test" "text")
                            (aux-data-data)
                            (gtirb-ir-sources *hello-world-gtirb*))
                      #'string<)))))

(deftest test-aux-data-update ()
  (with-fixture hello-world
    (is (member "text"
                (nest (hash-table-values)
                      (aux-data-upsert "Test" "text")
                      (aux-data-data)
                      (gtirb-ir-sources *hello-world-gtirb*))
                :test #'equalp))))

(deftest test-aux-data-remove ()
  (with-fixture hello-world
    (is (equalp '("Ghidra")
                (nest (hash-table-keys)
                      (aux-data-remove "Retdec")
                      (aux-data-data)
                      (gtirb-ir-sources *hello-world-gtirb*))))))
