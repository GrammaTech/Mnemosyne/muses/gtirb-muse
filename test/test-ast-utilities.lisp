(defpackage :gtirb-muse/test/test-ast-utilities
  (:use :gt/full
        :stefil+
        :functional-trees/attrs
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/c
        :gtirb-muse/ast-utilities
        :gtirb-muse/readtable
        :gtirb-muse/test/fixtures
        :gtirb-muse/test/utilities))
(in-package :gtirb-muse/test/test-ast-utilities)
(in-readtable gtirb-muse-readtable)

(defsuite test-ast-utilities "AST utilities")

(-> names (list) (values list &optional))
(defun names (asts)
  (if (every (of-type 'c-declaration) asts)
      (mappend #'declaration-names asts)
      (mapcar #'name asts)))

(deftest test-top-level-functions ()
  (with-fixture simple-crossover
    (is (equal (nest (names)
                     (top-level-functions)
                     (genome *crossover-1*))
               (list "main" "function1")))
    (is (equal (nest (names)
                     (top-level-functions)
                     (genome *crossover-2*))
               (list "main" "function2")))))

(deftest test-function-declarations ()
  (with-fixture simple-crossover
    (is (equal (nest (names)
                     (function-declarations)
                     (genome *crossover-1*))
               (list "main" "function1")))
    (is (equal (nest (names)
                     (function-declarations)
                     (genome *crossover-2*))
               (list "main" "function2")))))

(deftest test-type-declarations ()
  (with-fixture simple-crossover
    (is (equal (nest (names)
                     (type-declarations)
                     (genome *crossover-1*))
               (list "int32")))
    (is (equal (nest (names)
                     (type-declarations)
                     (genome *crossover-2*))
               (list "struct my_struct" "my_struct")))))

(deftest test-global-declarations ()
  (with-fixture simple-crossover
    (is (equal (nest (names)
                     (global-declarations)
                     (genome *crossover-1*))
               (list "global1")))
    (is (equal (nest (names)
                     (global-declarations)
                     (genome *crossover-2*))
               (list "global2")))))

(deftest test-header-declarations ()
  (with-fixture simple-crossover
    (is (null (nest (names)
                    (headers)
                    (genome *crossover-1*))))
    (is (equal (nest (names)
                     (headers)
                     (genome *crossover-2*))
               (list "stdio.h")))))

(deftest test-function-dependencies ()
  (with-fixture simple-crossover
    (with-attr-table *crossover-2*
      (let ((functions (top-level-functions (genome *crossover-2*))))
        (is (equal (names (function-dependencies (first functions) nil))
                   (list "main")))
        (is (equal (names (function-dependencies (first functions) functions))
                   (list "main" "function2")))
        (is (equal (names (function-dependencies (second functions) functions))
                   (list "function2")))))))

(deftest test-function-declaration-dependencies ()
  (with-fixture simple-crossover
    (let* ((root (genome *crossover-2*))
           (functions (top-level-functions root))
           (function-declarations (function-declarations root)))
      (is (null (function-declaration-dependencies nil nil)))
      (is (equal (names (function-declaration-dependencies
                         functions
                         function-declarations))
                 (list "main" "function2"))))))

(deftest test-type-declaration-dependencies ()
  (with-fixture simple-crossover
    (with-attr-table *crossover-2*
      (let ((type-declarations (type-declarations (genome *crossover-2*))))
        (is (equal (names (type-declaration-dependencies
                           (first type-declarations)
                           nil))
                   (list "struct my_struct")))
        (is (equal (names (type-declaration-dependencies
                           (first type-declarations)
                           type-declarations))
                   (list "struct my_struct")))
        (is (equal (names (type-declaration-dependencies
                           (second type-declarations)
                           type-declarations))
                   (list "my_struct" "struct my_struct")))))))

(deftest test-type-declarations-utilized ()
  (with-fixture simple-crossover
    (with-attr-table *crossover-2*
      (let* ((root (genome *crossover-2*))
             (functions (top-level-functions root))
             (global-declarations (global-declarations root))
             (type-declarations (type-declarations root)))
        (is (null (type-declarations-utilized nil nil nil)))
        (is (null (type-declarations-utilized nil nil type-declarations)))
        (is (equal (names (type-declarations-utilized functions
                                                      global-declarations
                                                      type-declarations))
                   (list "my_struct" "struct my_struct")))))))

(deftest test-global-declarations-utilized ()
  (with-fixture simple-crossover
    (with-attr-table *crossover-2*
      (let* ((root (genome *crossover-2*))
             (functions (top-level-functions root))
             (global-declarations (global-declarations root)))
        (is (null (global-declarations-utilized nil global-declarations)))
        (is (equal (names (global-declarations-utilized
                           (butlast functions)
                           global-declarations))
                   (list "global2")))
        (is (equal (names (global-declarations-utilized
                           (last functions)
                           global-declarations))
                   (list "global2")))))))
