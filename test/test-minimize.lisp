(defpackage :gtirb-muse/test/test-minimize
  (:use :gt/full
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/minimize
        :gtirb-muse/readtable
        :gtirb-muse/test/utilities))
(in-package :gtirb-muse/test/test-minimize)
(in-readtable gtirb-muse-readtable)

(defsuite test-minimize "Tests for minimization.")

(deftest test-minimize-1 ()
  (is (string= (nest (source-text)
                     (minimize-software)
                     (from-file (make 'c))
                     (path-join +etc-dir+ "crossover/simple/"
                                "crossover-non-recursive-result.c"))
               (nest (read-file-into-string)
                     (path-join +etc-dir+ "minimization/"
                                "minimize-result-1.c")))))

(deftest test-minimize-2 ()
  (is (string= (nest (source-text)
                     (minimize-software)
                     (from-file (make 'c))
                     (path-join +etc-dir+ "crossover/simple/"
                                "crossover-recursive-result.c"))
               (nest (read-file-into-string)
                     (path-join +etc-dir+ "minimization/"
                                "minimize-result-2.c")))))
