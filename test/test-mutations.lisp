(defpackage :gtirb-muse/test/test-mutations
  (:use :gt/full
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/c
        :gtirb-muse/mutations
        :gtirb-muse/readtable
        :gtirb-muse/test/utilities))
(in-package :gtirb-muse/test/test-mutations)
(in-readtable gtirb-muse-readtable)

(defsuite test-mutations "Tests for GTIRB muse mutations")

(deftest test-remove-unused-parameter-mutation-1 ()
  (let ((soft (from-string (make 'c)
                           "void foo() { foo(); }")))
    (is (signals no-mutation-targets
                 (nest (apply-mutation soft)
                       (make 'remove-unused-parameter :object soft))))))

(deftest test-remove-unused-parameter-mutation-2 ()
  (let ((soft (from-string (make 'c)
                           "void foo(int a) { foo(a); }")))
    (is (signals no-mutation-targets
                 (nest (apply-mutation soft)
                       (make 'remove-unused-parameter :object soft))))))

(deftest test-remove-unused-parameter-mutation-3 ()
  (let ((soft (from-string (make 'c)
                           "void foo(int a) { foo(0); }")))
    (is (string= (nest (genome-string)
                       (apply-mutation soft)
                       (make 'remove-unused-parameter :object soft))
                 "void foo() { foo(); }"))))

(deftest test-remove-unused-parameter-mutation-4 ()
  (let ((soft (from-string (make 'c)
                           "void foo(int a, int b) { foo(a, 0); }")))
    (is (string= (nest (genome-string)
                       (apply-mutation soft)
                       (make 'remove-unused-parameter :object soft))
                 "void foo(int a) { foo(a); }"))))

(deftest test-bind-callsite-return-1 ()
  (let ((soft (from-string (make 'c)
                           "int foo() { }")))
    (is (signals no-mutation-targets
                 (nest (apply-mutation soft)
                       (make 'bind-callsite-return :object soft))))))

(deftest test-bind-callsite-return-2 ()
  (let ((soft (from-string (make 'c)
                           (fmt "int foo() {~%~
                                     int extraout_EAX;~%~
                                     foo();~%~
                                 }"))))
    (is (string= (nest (genome-string)
                       (apply-mutation soft)
                       (make 'bind-callsite-return :object soft))
                 (fmt "int foo() {~%~
                           int extraout_EAX;~%~
                           extraout_EAX=foo();~%~
                       }")))))

(deftest test-bind-callsite-return-3 ()
  (let ((soft (from-string (make 'c)
                           (fmt "int foo() {~%~
                                     int extraout_EAX;~%~
                                     extraout_EAX=foo();~%~
                                 }"))))
    (is (string= (nest (genome-string)
                       (apply-mutation soft)
                       (make 'bind-callsite-return :object soft))
                 (genome-string soft)))))

(deftest test-non-void-return-1 ()
  "Test that running the non-void-return mutation on a software
object with no mutation targets signals a no-mutation-targets error."
  (let ((soft (nest (from-string 'c)
                    (fmt "void foo() { return; }"))))
    (is (signals no-mutation-targets
                 (nest (apply-mutation soft)
                       (make 'non-void-return :object soft))))))

(deftest test-non-void-return-2 ()
  "Test that running the non-void-return mutation on a software object
where the last statement in void function is a call to a non-void
function may be transformed such that the void function returns
a non-void value."
  (let ((soft (nest (from-string 'c)
                    (fmt "int bar() { return 0; }~%~
                          void foo() { bar(); }"))))
    (is (string= (nest (genome-string)
                       (apply-mutation soft)
                       (make 'non-void-return :object soft))
                 (fmt "int bar() { return 0; }~%~
                       int foo() { return bar(); }")))))

(deftest test-non-void-return-3 ()
  "Test that running the non-void-return mutation on a software object
where the last statement in void function prior to the return is a call
to a non-void function may be transformed such that the void function
returns a non-void value. This test also ensures function declarations
are also updated."
  (let ((soft (nest (from-string 'c)
                    (fmt "int bar();~%~
                          void foo();~%~
                          int bar() { return 0; }~%~
                          void foo() { bar(); return; }"))))
    (is (string= (nest (genome-string)
                       (apply-mutation soft)
                       (make 'non-void-return :object soft))
                 (fmt "int bar();~%~
                       int foo();~%~
                       int bar() { return 0; }~%~
                       int foo() { return bar(); }")))))
