(defpackage :gtirb-muse/test/test-code-lens
  (:use :gt/full
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/fitness
        :gtirb-muse/readtable
        :gtirb-muse/test/utilities)
  (:import-from :gtirb-muse/server
                :asm-function-definition-p
                :parse-asm-function-name
                :function-name-lineno-hashtable
                :function-fitness-hashtable))
(in-package :gtirb-muse/test/test-code-lens)
(in-readtable gtirb-muse-readtable)

(defsuite test-code-lens "Tests for LSP code lens functions."
  (and (which +gtirb-objdump+)
       (which +ddisasm+)))

(deftest test-asm-function-definition-p ()
  (is (not (asm-function-definition-p 0 (make-array 0))))
  (is (not (nest (asm-function-definition-p 0)
                 (nest (make-array 5 :initial-contents)
                       (list ".globl sprintf"
                             ".type sprintf, @function"
                             "#-----------------------------------"
                             ""
                             "")))))
  (is (nest (asm-function-definition-p 0)
            (nest (make-array 5 :initial-contents)
                  (list ".globl main"
                        ".type main, @function"
                        "#-----------------------------------"
                        "main:"
                        "")))))

(deftest test-parse-asm-function-name ()
  (is (equal "main" (parse-asm-function-name ".globl main"))))

(deftest test-function-name-lineno-hashtable-1 ()
  (is (equalp '(("hello" . 3) ("main" . 7))
              (sort (nest (hash-table-alist)
                          (function-name-lineno-hashtable)
                          (from-file (make 'c))
                          (path-join +etc-dir+ "hello/hello.c"))
                    #'< :key #'cdr))))

(deftest test-function-name-lineno-hashtable-2 ()
  (is (equalp '(("nullterm" . 0))
              (nest (hash-table-alist)
                    (function-name-lineno-hashtable)
                    (from-string (make 'c))
                    (fmt "char * nullterm () { return 0; }")))))

(deftest (test-function-fitness-hashtable :long-running) ()
  (let* ((hello-world (nest (from-file (make 'c))
                            (path-join +etc-dir+ "hello/hello.c")))
         (*disassembly-target* (function-instructions-hashtable hello-world)))
    (evaluate #'disassembly-test hello-world)
    (is (equalp '(("hello" . 1.0) ("main" . 1.0))
                (sort (nest (hash-table-alist)
                            (function-fitness-hashtable hello-world))
                      #'string< :key #'car)))))
