(defpackage :gtirb-muse/test/test-fitness
  (:use :gt/full
        :stefil+
        :software-evolution-library/software-evolution-library
        :software-evolution-library/components/test-suite
        :software-evolution-library/software/c
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/gtirb-utilities
        :gtirb-muse/fitness
        :gtirb-muse/readtable
        :gtirb-muse/test/fixtures
        :gtirb-muse/test/utilities)
  (:import-from :gtirb :read-gtirb))
(in-package :gtirb-muse/test/test-fitness)
(in-readtable gtirb-muse-readtable)


;;; Test suite definition.
(defsuite fitness-tests "Test GTIRB muse"
  (and (which +gtirb-objdump+)
       (which +ddisasm+)))


;;; Helper macros and functions.
(defmacro with-disassembly-fitness-test-bindings (gtirb-command &body body)
  `(let* ((*gtirb-command* ,gtirb-command)
          (*disassembly-target* (function-instructions-hashtable *hello-world*)))
     ,@body))

(defmacro with-behavioral-fitness-test-bindings (&body body)
  `(let ((*test-suite* (create-behavioral-tests *hello-world-test-script*)))
     ,@body))


;;; Tests for disassembling a binary/GTIRB file.
(deftest test-function-instructions-hashtable ()
  (with-fixture hello-world
    (let ((ht (function-instructions-hashtable *hello-world*)))
      (is (= 2 (hash-table-count ht)))
      (is (length= 12 (gethash "hello" ht)))
      (is (length= 23 (gethash "main" ht))))))


;;; Fitness function tests.

(deftest (test-perfect-disassembly-fitness-gtirb-objdump :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +gtirb-objdump+
      (test-perfect-disassembly-fitness))))

(deftest (test-perfect-disassembly-fitness-ddisasm :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +ddisasm+
      (test-perfect-disassembly-fitness))))

(defun test-perfect-disassembly-fitness ()
  (let ((fitness (disassembly-test *hello-world*)))
    (is (length= fitness (hash-table-count *disassembly-target*)))
    (is (every {= 1} fitness))))

(deftest (test-worst-disassembly-fitness-gtirb-objdump :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +gtirb-objdump+
      (test-worst-disassembly-fitness))))

(deftest (test-worst-disassembly-fitness-ddisasm :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +ddisasm+
      (test-worst-disassembly-fitness))))

(defun test-worst-disassembly-fitness ()
  (let ((fitness (disassembly-test (from-string (make 'c) ".{"))))
    (is (length= fitness (hash-table-count *disassembly-target*)))
    (is (every {= 0} fitness))))

(deftest (test-imperfect-disassembly-fitness-gtirb-objdump :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +gtirb-objdump+
      (test-imperfect-disassembly-fitness))))

(deftest (test-imperfect-disassembly-fitness-ddisasm :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +ddisasm+
      (test-imperfect-disassembly-fitness))))

(defun test-imperfect-disassembly-fitness ()
  (let ((fitness (disassembly-test (from-string (make 'c) "int main() {}"))))
    (is (length= fitness (hash-table-count *disassembly-target*)))
    (is (some {< 0} fitness))
    (is (every {> 1} fitness))))

(deftest (test-num-tests-invalid-script :long-running) ()
  (is (signals warning (num-tests (pathname "foo")))))

(deftest (test-num-tests-valid-script :long-running) ()
  (with-fixture hello-world
    (is (= 2 (num-tests *hello-world-test-script*)))))

(deftest (test-perfect-behavioral-fitness :long-running) ()
  (with-fixture hello-world
    (with-behavioral-fitness-test-bindings
      (let ((fitness (behavioral-test *hello-world*)))
        (is (length= fitness (test-cases *test-suite*)))
        (is (every {= 1} fitness))))))

(deftest (test-worst-behavioral-fitness-non-compilable :long-running) ()
  (with-fixture hello-world
    (with-behavioral-fitness-test-bindings
      (let* ((soft (from-string (make 'c) ".{"))
             (fitness (behavioral-test soft)))
        (is (length= fitness (test-cases *test-suite*)))
        (is (every {= 0} fitness))))))

(deftest (test-worst-behavioral-fitness-compilable :long-running) ()
  (with-fixture hello-world
    (with-behavioral-fitness-test-bindings
      (let* ((soft (from-string (make 'c) "int main() { return 1; }"))
             (fitness (behavioral-test soft)))
        (is (length= fitness (test-cases *test-suite*)))
        (is (every {= 0} fitness))))))

(deftest (test-imperfect-behavioral-fitness :long-running) ()
  (with-fixture hello-world
    (with-behavioral-fitness-test-bindings
      (let* ((soft (from-string (make 'c) "int main() { return 0; }"))
             (fitness (behavioral-test soft)))
        (is (length= fitness (test-cases *test-suite*)))
        (is (some {= 0} fitness))
        (is (some {= 1} fitness))))))

(deftest (test-fitness-test-lexicase :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +gtirb-objdump+
      (with-behavioral-fitness-test-bindings
        (let ((fitness (fitness-test-lexicase *hello-world*)))
          (is (length= fitness
                       (+ (hash-table-count *disassembly-target*)
                          (length (test-cases *test-suite*)))))
          (is (every {= 1} fitness)))))))

(deftest (test-fitness-test-multi-objective :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +gtirb-objdump+
      (with-behavioral-fitness-test-bindings
        (let ((fitness (fitness-test-multi-objective *hello-world*)))
          (is (length= (first fitness)
                       (hash-table-count *disassembly-target*)))
          (is (length= (second fitness)
                       (test-cases *test-suite*)))
          (is (every {= 1} (first fitness)))
          (is (every {= 1} (second fitness))))))))

(deftest (test-access-fitness-components :long-running) ()
  (with-fixture hello-world
    (with-disassembly-fitness-test-bindings +gtirb-objdump+
      (with-behavioral-fitness-test-bindings
        (evaluate #'fitness-test-lexicase *hello-world*)
        (is (length= (disassembly-fitness *hello-world*)
                     (hash-table-count *disassembly-target*)))
        (is (length= (behavioral-fitness *hello-world*)
                     (test-cases *test-suite*)))
        (is (every {= 1} (disassembly-fitness *hello-world*)))
        (is (every {= 1} (behavioral-fitness *hello-world*)))))))
