;;; A case-inverting readtable with support for curry-compose-reader-macros.
(defpackage :gtirb-muse/readtable
  (:use :gt/full)
  (:export :gtirb-muse-readtable)
  (:documentation "A case-inverting readtable with support for
curry-compose-reader-macros."))
(in-package :gtirb-muse/readtable)

(defreadtable gtirb-muse-readtable
  (:fuse :standard :curry-compose-reader-macros)
  (:case :invert))
