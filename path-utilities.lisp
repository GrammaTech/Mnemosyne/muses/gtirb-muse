;;; Path utilities
(defpackage :gtirb-muse/path-utilities
  (:use :quri
        :gt/full
        :lsp-server/protocol
        :lsp-server/utilities/lsp-utilities
        :gtirb-muse/readtable)
  (:export :gtirb-path-from-params
           :gtirb-path-from-uri
           :gtirb-path-from-file-path
           :asm-view-path-from-params
           :candidate-path-from-params
           :asm-view-path-p
           :candidate-path-p
           :gtirb-path-from-asm-view-path
           :gtirb-path-from-candidate-path
           :gtirb-path-from-cache-dir-path
           :cache-dir-p
           :cache-dir-from-asm-view-path
           :cache-dir-from-candidate-path
           :cache-dir-from-gtirb-path
           :candidate-path-from-gtirb-path
           :candidate-name-from-path
           :client-candidate-name-p))
(in-package :gtirb-muse/path-utilities)
(in-readtable gtirb-muse-readtable)

(-> gtirb-path-from-params (t) (values (or pathname null) &optional))
(defun gtirb-path-from-params (params &aux (path (path-from-params params)))
  "Return the path to the GTIRB file associated with the given LSP PARAMS."
  (gtirb-path-from-file-path path))

(-> gtirb-path-from-uri (string) (values (or pathname null) &optional))
(defun gtirb-path-from-uri (uri &aux (path (pathname (file-uri-path uri))))
  "Return the path to the GTIRB file associated with the given URI."
  (gtirb-path-from-file-path path))

(-> gtirb-path-from-file-path (pathname) (values (or pathname null) &optional))
(defun gtirb-path-from-file-path (path)
  "Return the path to the GTIRB file associated with the given PATH."
  (cond ((asm-view-path-p path)
         (gtirb-path-from-asm-view-path path))
        ((candidate-path-p path)
         (gtirb-path-from-candidate-path path))))

(-> asm-view-path-from-params (t) (or null pathname))
(defun asm-view-path-from-params (params &aux (path (path-from-params params)))
  "Return the path to the ASM view listing associated with the given
LSP PARAMS, if possible."
  (when (asm-view-path-p path) path))

(-> candidate-path-from-params (t) (or null pathname))
(defun candidate-path-from-params (params &aux (path (path-from-params params)))
  "Return the path to the candidate decompilation associated with the given
LSP PARAMS, if possible."
  (when (candidate-path-p path) path))

(-> asm-view-path-p (pathname) (values boolean &optional))
(defun asm-view-path-p (path)
  "Return T if PATH is a ASM view listing."
  (and (equal (pathname-type path) "view")
       (cache-dir-p (cache-dir-from-asm-view-path path))))

(-> candidate-path-p (pathname) (values boolean &optional))
(defun candidate-path-p (path)
  "Return T if PATH is a candidate decompilation."
  (and (equal (pathname-type path) "c")
       (cache-dir-p (cache-dir-from-candidate-path path))))

(-> gtirb-path-from-asm-view-path (pathname) (values pathname &optional))
(defun gtirb-path-from-asm-view-path (asm-view-path)
  "Return the path to the GTIRB file associated with the given
ASM view listing."
  (assert (asm-view-path-p asm-view-path))
  (nest (gtirb-path-from-cache-dir-path)
        (cache-dir-from-asm-view-path asm-view-path)))

(-> gtirb-path-from-candidate-path (pathname) (values pathname &optional))
(defun gtirb-path-from-candidate-path (candidate-path)
  "Return the path to the GTIRB file associated with the given
candidate decompilation."
  (assert (candidate-path-p candidate-path))
  (nest (gtirb-path-from-cache-dir-path)
        (cache-dir-from-candidate-path candidate-path)))

(-> gtirb-path-from-cache-dir-path (pathname) pathname)
(defun gtirb-path-from-cache-dir-path (cache-path)
  "Return the path to the GTIRB file associated with the given
cache directory."
  (assert (cache-dir-p cache-path))
  (make-pathname :defaults (pathname-parent-directory-pathname cache-path)
                 :name (nest (drop-suffix ".gtirb")
                             (drop-prefix ".vscode.")
                             (lastcar)
                             (pathname-directory cache-path))
                 :type "gtirb"
                 :version :newest))

(-> cache-dir-p (pathname) (values boolean &optional))
(defun cache-dir-p (path)
  "Return T if PATH is a GTIRB extension cache directory."
  (assert (directory-pathname-p path))
  (not (null (scan "^\\.?vscode\\..*\\.gtirb$"
                   (string (lastcar (pathname-directory path)))))))

(-> cache-dir-from-asm-view-path (pathname) (values pathname &optional))
(defun cache-dir-from-asm-view-path (asm-view-path)
  "Return the cache directory associated with the given ASM-VIEW-PATH."
  (assert (equal (pathname-type asm-view-path) "view"))
  (pathname-parent-directory-pathname asm-view-path))

(-> cache-dir-from-candidate-path (pathname) (values pathname &optional))
(defun cache-dir-from-candidate-path (candidate-path)
  "Return the cache directory associated with the given CANDIDATE-PATH."
  (assert (equal (pathname-type candidate-path) "c"))
  (pathname-directory-pathname candidate-path))

(-> cache-dir-from-gtirb-path (pathname) (values pathname &optional))
(defun cache-dir-from-gtirb-path (gtirb-path)
  "Return the cache directory associated with the given GTIRB-PATH."
  (assert (equal (pathname-type gtirb-path) "gtirb"))
  (path-join (pathname-directory-pathname gtirb-path)
             (fmt ".vscode.~a.~a/"
                  (pathname-name gtirb-path)
                  (pathname-type gtirb-path))))

(-> candidate-path-from-gtirb-path (pathname string) (values pathname &optional))
(defun candidate-path-from-gtirb-path (gtirb-path candidate-name)
  "Return the path to the candidate decompilation with the given CANDIDATE-NAME
from the given GTIRB-PATH."
  (path-join (cache-dir-from-gtirb-path gtirb-path)
             (if (client-candidate-name-p candidate-name)
                 (fmt "~a.c" candidate-name)
                 (fmt "~a_~a.c"
                      (pathname-name gtirb-path)
                      (string-downcase candidate-name)))))

(-> candidate-name-from-path (pathname) string)
(defun candidate-name-from-path (path &aux (name (pathname-name path)))
  "Return the name of the candidate decompilation at PATH."
  (assert (candidate-path-p path))
  (cond ((seed-candidate-name-p name)
         (string-capitalize (lastcar (split-sequence #\_ name)) :end 1))
        ((bed-candidate-name-p name)
         (string-downcase (lastcar (split-sequence #\_ name))))
        (t name)))

(-> client-candidate-name-p (string) boolean)
(defun client-candidate-name-p (candidate-name)
  "Return T if CANDIDATE-NAME is a candidate supplied by the client (user) and
not a default candidate present in the GTIRB (e.g. Retdec, Ghidra) or created
by this plugin (e.g. bed-[0-9]+)."
  (not (or (seed-candidate-name-p candidate-name)
           (bed-candidate-name-p candidate-name))))

(-> seed-candidate-name-p (string) boolean)
(defun seed-candidate-name-p (candidate-name)
  "Return T if CANDIDATE-NAME is a candidate from a seed decompilation."
  (not (null (member (string-downcase candidate-name)
                     (list "retdec" "ghidra" "dwarf")
                     :test (flip #'string$=)))))

(-> bed-candidate-name-p (string) boolean)
(defun bed-candidate-name-p (candidate-name)
  "Return T if CANDIDATE-NAME is a candidate created by the BED process."
  (not (null (scan "bed-([0-9]+|final)$" (string-downcase candidate-name)))))
