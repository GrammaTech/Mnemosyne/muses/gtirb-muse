;;; GTIRB utilities
(defpackage :gtirb-muse/gtirb-utilities
  (:use :gt/full
        :gtirb
        :gtirb-muse/readtable)
  (:shadowing-import-from :gtirb :size :symbol)
  (:export :+ir-sources+
           :+function-names+
           :+function-sources+
           :gtirb-ir-sources
           :gtirb-function-names
           :gtirb-function-blocks
           :gtirb-function-sources
           :gtirb-function-name-for-uuid
           :aux-data-upsert
           :aux-data-remove))
(in-package :gtirb-muse/gtirb-utilities)
(in-readtable gtirb-muse-readtable)

(defconst +ir-sources+ "irSources")
(defconst +function-names+ "functionNames")
(defconst +function-blocks+ "functionBlocks")
(defconst +function-sources+ "functionSources")

(-> gtirb-ir-sources (gtirb) (values (or aux-data null) &optional))
(defun gtirb-ir-sources (gtirb)
  "Return the ir sources aux data for GTIRB."
  (aget +ir-sources+ (aux-data gtirb) :test #'equal))

(-> gtirb-function-names (module) (values (or aux-data null) &optional))
(defun gtirb-function-names (module)
  "Return the function names aux data for MODULE."
  (aget +function-names+ (aux-data module) :test #'equal))

(-> gtirb-function-blocks (module) (values (or aux-data null) &optional))
(defun gtirb-function-blocks (module)
  "Return the function blocks aux-data for MODULE."
  (aget +function-blocks+ (aux-data module) :test #'equal))

(-> gtirb-function-sources (module) (values (or aux-data null) &optional))
(defun gtirb-function-sources (module)
  "Return the function sources aux data for MODULE."
  (aget +function-sources+ (aux-data module) :test #'equal))

(-> gtirb-function-name-for-uuid (gtirb number)
                                 (values (or null string) &optional))
(defun gtirb-function-name-for-uuid (gtirb uuid)
  "Return the function name associated with the given UUID in GTIRB."
  (iter (for module in (modules gtirb))
        (when-let ((sym (get-uuid (nest (gethash uuid)
                                        (aux-data-data)
                                        (gtirb-function-names module))
                                  gtirb)))
          (return (name sym)))))

(-> aux-data-upsert (t t hash-table &key (:test function))
                    (values hash-table &optional))
(defun aux-data-upsert (key value hashtable &key (test #'equal))
  "Insert/update the given KEY/VALUE pair in the aux-data HASHTABLE."
  (dict* (copy-hash-table hashtable :test test) key value))

(-> aux-data-remove (t hash-table &key (:test function))
                    (values hash-table &optional))
(defun aux-data-remove (key hashtable &key (test #'equal))
  "Remove the entry with the given KEY in the aux-data HASHTABLE."
  (delete-from-hash-table (copy-hash-table hashtable :test test) key))
