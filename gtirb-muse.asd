(defsystem "gtirb-muse"
  :description "Mnemosyne muse for interacting with GTIRB files."
  :author "GrammaTech"
  :depends-on (gtirb-muse/server)
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :in-order-to ((test-op (load-op "gtirb-muse/test")))
  :perform (test-op (o c) (symbol-call :gtirb-muse/test '#:run-batch)))

(defsystem "gtirb-muse/run-gtirb-muse-server"
  :description "Command-line interface to GTIRB LSP server."
  :author "GrammaTech"
  :depends-on (gtirb-muse/server)
  :build-operation "asdf:program-op"
  :build-pathname "bin/gtirb-muse-server"
  :entry-point "gtirb-muse/server::run-server")

(register-system-packages "lsp-server" '(:lsp-server/protocol))
(register-system-packages "lsp-server" '(:lsp-server/protocol-util))
(register-system-packages "lsp-server" '(:lsp-server/lsp-server))
(register-system-packages "lsp-server" '(:lsp-server/logger))
(register-system-packages "lsp-server/lem-base" '(:lsp-server.lem-base))
(register-system-packages "lsp-server/utilities" '(:lsp-server/utilities/lsp-utilities))
(register-system-packages "lsp-server/utilities" '(:lsp-server/utilities/worker-thread))
