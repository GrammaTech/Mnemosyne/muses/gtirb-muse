;;; Minimization
(defpackage :gtirb-muse/minimize
  (:use :gt/full
        :functional-trees/attrs
        :functional-trees/sel-attributes
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/ast-utilities
        :gtirb-muse/readtable)
  (:export :minimize-software))
(in-package :gtirb-muse/minimize)
(in-readtable gtirb-muse-readtable)

(defgeneric minimize-software (software)
  (:documentation "Minimize SOFTWARE by removing unused
functions, globals, and types.")
  (:method ((software c))
    (with-attr-table (genome software)
      (nest (minimize-type-declarations)
            (minimize-global-declarations)
            (minimize-functions software))))
  (:method :around ((software c))
    (multiple-value-call (lambda (minimized &rest rest)
                           (setf (fitness minimized) nil)
                           (apply #'values minimized rest))
      (call-next-method))))

(-> minimize-functions (c) (values c &optional))
(defun minimize-functions (software &aux (root (genome software)))
  "Minimize SOFTWARE by removing unused functions."
  (let ((minimized (minimize-functions-helper root)))
    (funcall (if (eq minimized root)
                 #'identity
                 #'minimize-functions)
             (copy software :genome minimized))))

(-> minimize-functions-helper (root-ast) (values root-ast &optional))
(defun minimize-functions-helper (root)
  "Helper function performing a single round of minimization by removing
unused functions from ROOT."
  (let* ((functions (top-level-functions root))
         (function-declararations (function-declarations root))
         (names (reduce #'union functions
                        :key [#'uses #'function-body]
                        :initial-value (set "main"))))
    (reduce (lambda (root ast)
              (let ((name (if (listp (c-declarator ast))
                              (name (first (c-declarator ast)))
                              (name (c-declarator ast)))))
                (if (find name names :test #'string=)
                    root
                    (less root ast))))
            (append functions function-declararations)
            :initial-value root)))

(-> minimize-global-declarations (c) (values c &optional))
(defun minimize-global-declarations (software &aux (root (genome software)))
  "Minimize SOFTWARE by removing unused global variable declarations."
  (minimize-helper software
                   #'global-declarations
                   {global-declarations-utilized (top-level-functions root)}))

(-> minimize-type-declarations (c) (values c &optional))
(defun minimize-type-declarations (software &aux (root (genome software)))
  "Minimize SOFTWARE by removing unused type declarations."
  (minimize-helper software
                   #'type-declarations
                   {type-declarations-utilized (top-level-functions root)
                                               (global-declarations root)}))

(-> minimize-helper (c function function)
                    (values c &optional))
(defun minimize-helper (software declarations-fn utilization-fn
                        &aux (root (genome software)))
  "Helper function extracting common pieces for minimizing global
variable and type declarations in SOFTWARE."
  (let ((declarations (funcall declarations-fn root)))
    (nest (copy software :genome)
          (reduce (lambda (root global)
                    (less root global))
                  (nest (set-difference declarations)
                        (funcall utilization-fn declarations))
                  :initial-value root))))
