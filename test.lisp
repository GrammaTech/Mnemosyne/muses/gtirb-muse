(defpackage :gtirb-muse/test
  (:use :gt/full
        :stefil+
        :gtirb-muse/readtable
        :gtirb-muse/test/test-ast-utilities
        :gtirb-muse/test/test-bed
        :gtirb-muse/test/test-code-lens
        :gtirb-muse/test/test-crossover
        :gtirb-muse/test/test-fitness
        :gtirb-muse/test/test-gtirb-utilities
        :gtirb-muse/test/test-minimize
        :gtirb-muse/test/test-mutations
        :gtirb-muse/test/test-path-utilities)
  (:export :test :run-batch))
(in-package :gtirb-muse/test)
(in-readtable gtirb-muse-readtable)
