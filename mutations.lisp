;;; Mutations for GTIRB muse
(defpackage :gtirb-muse/mutations
  (:use :gt/full
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/software/c
        :gtirb-muse/ast-utilities
        :gtirb-muse/readtable)
  (:import-from :software-evolution-library/software/tree-sitter
                :declarator-name-ast)
  (:export :remove-unused-parameter
           :bind-callsite-return
           :non-void-return))
(in-package :gtirb-muse/mutations)
(in-readtable gtirb-muse-readtable)

(define-mutation gtirb-muse-mutation (parseable-mutation) ()
  (:documentation "Base class for all GTIRB muse mutations.")
  (:default-initargs
   :picker #'gtirb-muse-mutation-picker))

(define-mutation remove-unused-parameter (gtirb-muse-mutation)
  ((targeter :initform #'remove-unused-parameter-targeter))
  (:documentation "Remove an unused parameter from a function."))

(define-mutation bind-callsite-return (gtirb-muse-mutation)
  ((targeter :initform #'bind-callsite-return-targeter))
  (:documentation "If possible, bind or rebind a bindable variable to a
callsite return value."))

(define-mutation non-void-return (gtirb-muse-mutation)
  ((targeter :initform #'non-void-return-targeter))
  (:documentation "If possible, change the return type of a void function to
a non-void type."))

(-> gtirb-muse-mutation-picker (gtirb-muse-mutation) t)
(defun gtirb-muse-mutation-picker (mutation &aux (targets (targets mutation)))
  "Return a random MUTATION target, if possible."
  (if targets
      (random-elt targets)
      (error 'no-mutation-targets
        :obj (object mutation)
        :text "No suitable ASTs for mutation.")))

(defgeneric remove-unused-parameter-targeter (obj)
  (:documentation "Return target ASTs for the remove-unused-parameter mutation.")
  (:method ((obj software))
    (remove-unused-parameter-targeter (genome obj)))
  (:method ((root root-ast))
    (functions-with-unused-parameter root)))

(defgeneric bind-callsite-return-targeter (obj)
  (:documentation "Return target ASTs for the bind-callsite-return mutation.")
  (:method ((obj software))
    (bind-callsite-return-targeter (genome obj)))
  (:method ((root root-ast))
    (let* ((functions (top-level-functions root))
           (non-void-functions (remove-if #'void-function-p functions)))
      (mappend «and {bindable-declarators}
                    {callsites-to-bind-return _ non-void-functions}»
               functions))))

(defgeneric non-void-return-targeter (obj)
  (:documentation "Return target ASTs for the non-void-return mutation.")
  (:method ((obj software))
    (non-void-return-targeter (genome obj)))
  (:method ((root root-ast) &aux (functions (top-level-functions root)))
    (remove-if-not «and #'void-function-p
                        {ends-with-call-to-non-void-function-p _ functions}»
                   functions)))

(defmethod build-op ((mutation remove-unused-parameter)
                     (software c))
  "Return a sequence of primitive mutation operations implementing the
removal of an unused parameter from a function."
  (build-op mutation (genome software)))

(defmethod build-op ((mutation remove-unused-parameter)
                     (root root-ast))
  "Return a sequence of primitive mutation operations implementing the
removal of an unused parameter from a function."
  (let* ((function (funcall (picker mutation) mutation))
         (function-decl (find function (function-declarators root)
                              :test #'name-equal))
         (parameter-index (position (random-elt (unused-parameters function))
                                    (parameter-identifiers function))))
    (nest (mapcar (lambda (ast) `(:cut (:stmt1 . ,ast))))
          (remove-if #'null)
          (cons (nth parameter-index (function-parameters function)))
          (cons (nth parameter-index (if function-decl
                                         (function-parameters function-decl)
                                         nil)))
          (mapcar [{nth parameter-index} #'call-arguments])
          (function-callsites root function))))

(defmethod build-op ((mutation bind-callsite-return)
                     (software c))
  "Return a sequence of primitive mutation operations implementing the binding
or rebinding of a variable to the result of a function call."
  (build-op mutation (genome software)))

(defmethod build-op ((mutation bind-callsite-return)
                     (root root-ast))
  "Return a sequence of primitive mutation operations implementing the binding
or rebinding of a variable to the result of a function call."
  (let* ((callsite (funcall (picker mutation) mutation))
         (target (get-parent-ast root callsite))
         (decl (nest (copy-ast-only)
                     (random-elt)
                     (bindable-declarators)
                     (find-enclosing 'function-ast root target))))
    `((:set (:stmt1 . ,target)
            (:literal1 . ,(if (typep target 'assignment-ast)
                              (copy target :c-left decl)
                              (nest (copy target :children)
                                    (list)
                                    (convert 'c-ast)
                                    `((:class . :assignment-expression)
                                      (:left . ,decl)
                                      (:right . ,(copy-ast-only callsite))
                                      (:operator . ((:class . :=)
                                                    (:text . "=")))))))))))

(defmethod build-op ((mutation non-void-return)
                     (software c))
  "Return a sequence of primitive mutation operations implementing the changing
of a void function to non-void."
  (build-op mutation (genome software)))

(defmethod build-op ((mutation non-void-return)
                     (root root-ast))
  "Return a sequence of primitive mutation operations implementing the changing
of a void function to non-void."
  (let* ((function (funcall (picker mutation) mutation))
         (function-decl (find function (function-declarations root)
                              :key [#'first #'c-declarator]
                              :test #'name-equal))
         (body (c-body function))
         (body-stmts (direct-children body))
         (expression (lastcar (take-until (of-type 'return-ast) body-stmts)))
         (callsite (call-ast-from-expression-statement expression))
         (call-function (nest (callsite-function callsite)
                              (top-level-functions root))))
    (append (nest (list)
                  (list :set (cons :stmt1 function))
                  (cons :literal1)
                  (copy function :c-type (c-type call-function) :c-body)
                  (copy body :children)
                  (append (take-until {eq expression} body-stmts))
                  (list)
                  (convert 'c-ast)
                  (append `((:class . :return-statement)
                            (:before-text . ,(before-text expression))
                            (:after-text . ,(after-text (lastcar body-stmts)))))
                  (list)
                  (cons :children)
                  (list)
                  (copy-ast-only callsite :before-text " "))
            (when function-decl
              (nest (list)
                    (list :set (cons :stmt1 function-decl))
                    (cons :literal1 (nest (copy function-decl :c-type)
                                          (c-type call-function))))))))

(-> callsites-to-bind-return (function-ast list)
                             (values list &optional))
(defun callsites-to-bind-return (function non-void-functions)
  "Return a list of callsites calling NON-VOID-FUNCTIONS in FUNCTION where the
return value may be bound or rebound to a variable."
  (nest (remove-if-not {callsite-function _ non-void-functions})
        (remove-if-not {expression-or-assignment-to-rebindable-call-p function})
        (remove-if-not (of-type 'call-ast))
        (convert 'list function)))

(-> expression-or-assignment-to-rebindable-call-p (ast call-ast)
                                                  (values boolean &optional))
(defun expression-or-assignment-to-rebindable-call-p (root call)
  "Return non-NIL if the parent of CALL in ROOT is an expression statement AST
or an assignment to a rebindable variable."
  (let ((parent (get-parent-ast root call)))
    (or (typep parent 'expression-statement-ast)
        (and (typep parent 'assignment-ast)
             (bindable-identifier-p (c-left parent))))))

(-> bindable-declarators (function-ast) (values list &optional))
(defun bindable-declarators (function)
  "Return the declarations of variables which are bindable for callsite
return values in FUNCTION."
  (nest (remove-if-not #'bindable-identifier-p)
        (filter-map #'declarator-name-ast)
        (convert 'list function)))

(-> bindable-identifier-p (ast) (values boolean &optional))
(defun bindable-identifier-p (ast)
  "Return non-NIL if AST is a bindable identifier for a callsite return value."
  (starts-with-subseq "extraout" (source-text ast)))

(-> ends-with-call-to-non-void-function-p (function-ast list)
                                          (values boolean &optional))
(defun ends-with-call-to-non-void-function-p (function functions)
  "Return T if FUNCTION ends with a call to a non-void function in FUNCTIONS."
  (and-let* ((body-stmts (nest (take-until (of-type 'return-ast))
                               (direct-children)
                               (c-body function)))
             (call (call-ast-from-expression-statement (lastcar body-stmts)))
             (call-function (callsite-function call functions))
             ((not (void-function-p call-function))))))

(-> call-expression-statement-p (ast) (values boolean &optional))
(defun call-expression-statement-p (ast)
  "Return T if AST is an expression statement with a single function call
AST child."
  (and (typep ast 'expression-statement-ast)
       (length= 1 (children ast))
       (typep (first (children ast)) 'call-ast)))

(-> call-ast-from-expression-statement (ast)
                                       (values (or call-ast null) &optional))
(defun call-ast-from-expression-statement (ast)
  "Return the function call in AST if AST an expression statement with
a single function call AST child."
  (when (call-expression-statement-p ast)
    (first (children ast))))
