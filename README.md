# GTIRB/Behavioral Equivalent Decompilation (BED) LSP Server

This is a standalone LSP server for use with the associated
GTIRB/Behavioral Equivalent Decompilation (BED) VSCode extension.
The server and associated VSCode extension allow clients to
interact with GTIRB files annotated with candidate decompilations,
including starting an evolutionary process to improve upon these
decompilations.

Instructions on installing and running the VSCode plugin (and
how it interacts with this repository) may be found at
[https://git.grammatech.com/jruchti/gtirb-bed-vscode](https://git.grammatech.com/jruchti/gtirb-bed-vscode).
This should be the quickstart for most clients.

For developers of this LSP server, please consult the Dockerfile
for instructions on locally installing required dependencies.
