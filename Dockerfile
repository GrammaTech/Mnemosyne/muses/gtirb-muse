FROM grammatech/sel

# Install base GTIRB packages from apt/pypi
RUN wget -O - https://download.grammatech.com/gtirb/files/apt-repo/conf/apt.gpg.key | apt-key add -
RUN echo "deb https://download.grammatech.com/gtirb/files/apt-repo focal stable"| tee -a /etc/apt/sources.list
RUN apt update && \
    apt-get install -y --fix-missing libgtirb gtirb-pprinter ddisasm && \
    pip3 install gtirb

# Install protocol buffers for Common Lisp
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
RUN apt-get install -y libtool && \
    cd /tmp/ && \
    git clone https://github.com/protocolbuffers/protobuf.git google-protobuf && \
    cd google-protobuf && \
    git checkout v3.19.4 && \
    ./autogen.sh && \
    ./configure && \
    make && \
    make install && \
    cd - && \
    rm -rf google-protobuf && \
    git clone https://github.com/brown/protobuf.git && \
    cd protobuf/protoc/lisp && \
    PROTOC_ROOT=/usr/local/ INSTALL_ROOT=/usr/local/ make install && \
    cd - && \
    rm -rf protobuf

# Build the GTIRB muse LSP server
COPY . /root/quicklisp/local-projects/gtirb-muse
WORKDIR /root/quicklisp/local-projects/gtirb-muse
RUN pip3 install .python-wheels/gt_binprep-0.4.dev0-py3-none-any.whl && \
    pip3 install .python-wheels/gtirb_objdump-0.1.dev0-py3-none-any.whl
RUN make

ENV PATH=/root/quicklisp/local-projects/gtirb-muse/bin:$PATH
EXPOSE 9988
CMD gtirb-muse-server --port 9988
