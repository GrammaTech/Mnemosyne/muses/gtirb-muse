;;; Fitness function
(defpackage :gtirb-muse/fitness
  (:use :cmd
        :diff
        :capstone
        :lparallel
        :gt/full
        :gtirb
        :software-evolution-library/software-evolution-library
        :software-evolution-library/command-line
        :software-evolution-library/components/test-suite
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :software-evolution-library/utility/debug
        :gtirb-muse/ast-utilities
        :gtirb-muse/gtirb-utilities
        :gtirb-muse/readtable)
  (:shadowing-import-from :gtirb :address :bytes :version :size :symbol :name)
  (:shadowing-import-from :uiop/stream
                          :with-temporary-file
                          :*temporary-directory*)
  (:import-from :uiop/run-program :run-program :subprocess-error)
  (:export :+gtirb-objdump+
           :+ddisasm+
           :+limit-path+
           :+default-test-case-timeout+
           :*fitness-test*
           :*disassembly-target*
           :*test-suite*
           :*gtirb-command*
           :with-temporary-script-of
           :parallel-evaluate
           :evaluate-threadsafe
           :fitness-test-lexicase
           :fitness-test-multi-objective
           :disassembly-fitness
           :behavioral-fitness
           :disassembly-test
           :behavioral-test
           :worst-fitness-p
           :target-fitness-p
           :fitness-predicate
           :function-instructions-hashtable
           :create-behavioral-tests
           :num-tests))
(in-package :gtirb-muse/fitness)
(in-readtable gtirb-muse-readtable)

(defconst +gtirb-objdump+ "gtirb-objdump")
(defconst +ddisasm+ "ddisasm")
(defconst +limit-path+
          (asdf:system-relative-pathname :gtirb-muse "bin/limit"))
(defconst +default-test-case-timeout+ (1- (expt 2 31)))

(defvar *fitness-test* nil
  "Variable for lexically binding an evolutionary fitness test.")

(defvar *disassembly-target* (make-hash-table)
  "Hashtable mapping function names to instructions in the target binary.")

(defvar *test-suite* (make 'test-suite)
  "Test suite for evaluating the behavioral fitness of a binary.")

(defvar *gtirb-command* +gtirb-objdump+
  "Command to use to create a GTIRB file from a binary.")

(defconst +crt-functions+
  '("__cxa_allocate_exception"
    "__cxa_begin_catch"
    "__cxa_end_catch"
    "__cxa_throw"
    "__cxa_finalize"
    "deregister_tm_clones"
    "_dl_relocate_static_pie"
    "__do_global_ctors_aux"
    "__do_global_dtors_aux"
    "_fini"
    "frame_dummy"
    "__gmon_start__"
    "__i686_get_pc_thunk_bx"
    "__i686.get_pc_thunk.bx"
    "_init"
    "__JCR_END__"
    "__libc_csu_fini"
    "__libc_csu_init"
    "__libc_start_main"
    "register_tm_clones"
    "__stack_chk_fail"
    "_start"
    "__TMC_END__"
    "__udivdi3"
    "__umoddi3"
    "_Unwind_Resume"
    "__x86_get_pc_thunk_bx"
    "__x86.get_pc_thunk.bx"
    "_ZSt16__throw_bad_castv")
  "List of C runtime (CRT) functions common to all binaries.")

(defmacro with-temporary-script-of
    ((&rest args &key (pathname (gensym "PATHNAME")) &allow-other-keys)
     string &body body)
  "Execute BODY with STRING in a temporary executable script whose path
is bound to PATHNAME.  Additional keyword args are passed thru to
`with-temporary-file`."
  `(with-temporary-file (,@args)
     (when (and ,pathname ,string)
       (string-to-file ,string ,pathname)
       (setf (file-permissions ,pathname)
             (append (file-permissions ,pathname)
                     (list :user-exec :group-exec :other-exec))))
     (progn ,@body)))

(defmacro with-temporary-gtirb-file-for-binary ((gtirb-path binary)
                                                &body body)
  "Execute BODY with GTIRB-PATH bound to a temporary gtirb path for the
given BINARY."
  `(let* ((,gtirb-path (nest (make-pathname :defaults ,binary :type)
                             (fmt "~a.gtirb" (pathname-type ,binary)))))
     (unwind-protect (progn ,@body)
       (delete-file-if-exists ,gtirb-path))))

(defmacro with-decoding-error-handling (&body body)
  "Execute BODY with error handling for stream decoding errors."
  #+sbcl
  `(handler-bind ((sb-int:stream-decoding-error
                   (lambda (e)
                     (declare (ignorable e))
                     (invoke-restart 'sb-int:force-end-of-file))))
     (progn ,@body))
  #-sbcl
  `(progn ,@body))

(defmacro with-num-tests-error-handling (&body body)
  "Execute BODY with error handling for running the `num-tests` command
on gt-harness scripts."
  `(handler-case
       (progn ,@body)
     (parse-error (e)
       (declare (ignorable e))
       (warn "Harness script does not implement the `num-tests` command."))
     (subprocess-error (e)
       (declare (ignorable e))
       (warn "Harness script does not implement the `num-tests` command."))))

(defmacro with-fitness-interrupt-error-handling (&body body)
  "Execute the body of the fitness function in a handler to protect against
interrupts."
  `(handler-case
       (progn ,@body)
     (simple-error (e)
       (note 1 "Error in fitness function: ~a~%" e)
       (list (make-array (hash-table-count disassembly-target)
                         :initial-element 0)
             (make-array (length (test-cases test-suite))
                         :initial-element 0)))))

(-> parallel-evaluate (function list) (values list &optional))
(defun parallel-evaluate (test new-children)
  "Evaluate NEW-CHILDREN using TEST assigning each a fitness."
  (setf *fitness-evals* (+ *fitness-evals* (length new-children)))
  (pmap 'list {evaluate test} new-children))

(-> evaluate-threadsafe (function c &rest list &key &allow-other-keys) t)
(defun evaluate-threadsafe (test obj &rest extra-keys &key &allow-other-keys)
  "Evaluate OBJ using TEST returning a fitness value."
  (synchronized (obj)
    (apply #'evaluate test obj extra-keys)))

(-> fitness-test-lexicase (c &rest list) (values vector &optional))
(defun fitness-test-lexicase (obj &rest args)
  "Compare the disassembly of OBJ with the DISASSEMBLY-TARGET and execute
TEST-SUITE on OBJ, returning a concatenated vector with values between 0
and 1 representing degree of fitness (greater values being better fitness)."
  (nest (apply #'concatenate 'simple-vector)
        (apply #'fitness-test-multi-objective obj args)))

(-> fitness-test-multi-objective (c &optional hash-table test-suite)
                                 (values list &optional))
(defun fitness-test-multi-objective
    (obj &optional (disassembly-target *disassembly-target*)
                   (test-suite *test-suite*))
  "Compare the disassembly of OBJ with the DISASSEMBLY-TARGET and execute
TEST-SUITE on OBJ, returning two vectors with values between 0 and 1
representing degree of fitness (greater values being better fitness)."
  (with-fitness-interrupt-error-handling
    (list (disassembly-test obj disassembly-target)
          (behavioral-test obj test-suite))))

(-> disassembly-fitness (c) (values vector &optional))
(defun disassembly-fitness (obj)
  "Return the component of OBJ's fitness representing the similarity of
disassembly between OBJ and the target."
  (assert (fitness obj) () "Object must have a fitness.")
  (subseq (fitness obj) 0 (hash-table-count *disassembly-target*)))

(-> behavioral-fitness (c) (values vector &optional))
(defun behavioral-fitness (obj)
  "Return the component of OBJ's fitness representing the performance
of OBJ on a test suite."
  (assert (fitness obj) () "Object must have a fitness.")
  (subseq (fitness obj) (hash-table-count *disassembly-target*)))

(-> disassembly-test (c &optional hash-table) (values vector &optional))
(defun disassembly-test (obj &optional (disassembly-target *disassembly-target*))
  "Compare the disassembly of OBJ with the disassembly-target,
distilling the results into a list of fitness values for each
function in OBJ.  A fitness of 1.0 indicates a perfect function
match, while 0.0 indicates no match whatsoever."
  (if-let ((variant (function-instructions-hashtable obj))
           (functions (hash-table-keys disassembly-target)))
    (iter (for function-name in functions)
          (for target-instructions = (gethash function-name disassembly-target))
          (for variant-instructions = (gethash function-name variant))
          (collect (diff-similarity (extract-disassembly target-instructions)
                                    (extract-disassembly variant-instructions))
                   into fitness)
          (finally (return (coerce fitness 'vector))))
    (make-array (length functions) :initial-element 0)))

(-> behavioral-test (c &optional test-suite) (values vector &optional))
(defun behavioral-test (obj &optional (test-suite *test-suite*))
  "Execute TEST-SUITE on OBJ, returing a list of binary fitness values
indicates success (1) or failure (0) on each test case in suite. The test
suite is evaluated NUM-TEST-RUNS times to tease out stochastic test failures."
  (with-temporary-file-of (:pathname source :type "c") (genome-string obj)
    (with-temporary-file (:pathname bin)
      (with-decoding-error-handling
        (ignore-phenome-errors
          (if-let ((bin (phenome obj :bin bin)))
            (nest (funcall {coerce _ 'vector})
                  (mapcar (lambda (test-case)
                            (evaluate bin test-case
                                      :output nil
                                      :error-output nil)))
                  (test-cases test-suite))
            (make-array (length (test-cases test-suite)) :initial-element 0)))))))

(-> worst-fitness-p (c) (values boolean &optional))
(defun worst-fitness-p (obj)
  "Return true if OBJ has the worst possible fitness."
  (or (null (fitness obj)) (every #'zerop (fitness obj))))

(-> target-fitness-p (c) boolean)
(defun target-fitness-p (obj)
  "Return true if OBJ has the target fitness."
  (<= 1 (mean (fitness obj))))

(-> fitness-predicate (list list) boolean)
(defun fitness-predicate (fitness-a fitness-b)
  "Return true if FITNESS-A is strictly better than FITNESS-B."
  (> (funcall *fitness-scalar-fn* fitness-a)
     (funcall *fitness-scalar-fn* fitness-b)))

(defgeneric function-instructions-hashtable (software)
  (:documentation "Return a hashtable mapping function name -> assembly
instructions for functions in SOFTWARE.")
  (:method ((c c))
    (with-temporary-file (:pathname bin)
      (with-temporary-gtirb-file-for-binary (gtirb-path bin)
        (with-current-directory (*temporary-directory*)
          (ignore-phenome-errors
            (phenome c :bin bin)
            (apply #'cmd? *gtirb-command*
                   (if (equal *gtirb-command* +gtirb-objdump+)
                       (list "--func-patterns" bin)
                       (list (fmt "--ir=~a" gtirb-path) bin)))
            (if (file-exists-p gtirb-path)
                (function-instructions-hashtable (read-gtirb gtirb-path))
                (make-hash-table)))))))
  (:method ((gtirb gtirb))
    (nest (apply #'merge-tables)
          (mapcar #'function-instructions-hashtable)
          (modules gtirb)))
  (:method ((module module)
            &aux (function-names (aux-data-data (gtirb-function-names module)))
              (function-blocks (aux-data-data (gtirb-function-blocks module)))
              (engine (make-instance 'capstone-engine
                        :architecture :x86
                        :mode :64)))
    (iter (for (uuid entry) in-hashtable function-names)
          (and-let* ((name (if (listp entry)
                               (first entry)
                               (name (get-uuid entry (ir module)))))
                     (blocks (sort (nest (mapcar {get-uuid _ module})
                                         (gethash uuid function-blocks))
                                   #'<
                                   :key #'address))
                     ((not (crt-function-p name)))
                     ((text-section-p blocks)))
            (collect (cons name (mappend {parse-instructions engine} blocks))
                     into result)
            (finally (return (alist-hash-table result :test #'equal)))))))

(-> crt-function-p (string) (values (or string null) &optional))
(defun crt-function-p (function-name)
  "Return non-nil if FUNCTION-NAME is a C runtime (CRT) function."
  (find function-name +crt-functions+ :test #'equal))

(-> text-section-p (list) (values boolean &optional))
(defun text-section-p (blocks)
  "Return true if every block in BLOCKS appears in the .text
section of the binary."
  (and blocks
       (every [{equal ".text"} #'name #'section #'byte-interval] blocks)))

(-> parse-instructions (capstone-engine code-block) list)
(defun parse-instructions (engine code-block)
  "Parse assembly instructions from raw bytes in a GTIRB code block."
  (coerce (disasm engine (bytes code-block)) 'list))

(-> extract-disassembly (list) (values list &optional))
(defun extract-disassembly (instructions)
  "Return a list of disassembly (mnemonic . operarands) pairs for each
instruction in INSTRUCTIONS."
  (mapcar (lambda (instruction)
            (cons (mnemonic instruction) (operands instruction)))
          instructions))

(-> diff-similarity (list list) (real 0 1))
(defun diff-similarity (target variant)
  "Return a measure of the similarity between TARGET and VARIANT as a float
in the range [0, 1].  Where T is the total number of elements in both
sequences, and M is the number of matches, this is 2.0*M / T.  Note this
is 1.0 if the sequences are identical, and 0.0 if they have nothing in
common."
  (/ (nest (reduce #'+)
           (mapcar [{* 2} #'original-length])
           (remove-if-not (of-type 'common-diff-region))
           (compute-raw-seq-diff target variant))
     (+ (length target) (length variant))))

(-> create-behavioral-tests (pathname &key (:timeout fixnum))
                            (values test-suite &optional))
(defun create-behavioral-tests
    (test-script-path &key (timeout +default-test-case-timeout+))
  "Create a test suite for the test harness script at TEST-SCRIPT-PATH
with the given per-test-case TIMEOUT.  This function assumes
TEST-SCRIPT-PATH follows the conventions of gt-harness scripts and
implements a `test` command."
  (create-test-suite (fmt "~a test ~~a ~~d" test-script-path)
                     (num-tests test-script-path)
                     :limit-path +limit-path+
                     :time-limit timeout))

(-> num-tests (pathname) fixnum)
(defun num-tests (test-script-path)
  "Return the total number of test cases for the test harness script at
TEST-SCRIPT-PATH.  This function assumes TEST-SCRIPT-PATH follows the
conventions of gt-harness scripts and implements a `num-tests` command."
  (or (with-num-tests-error-handling
        (nest (parse-integer)
              (funcall {run-program _ :output '(:string :stripped t)})
              (fmt "~a num-tests" test-script-path)))
      0))
