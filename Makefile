SHELL := /bin/bash

# Set personal or machine-local flags in a file named local.mk
ifneq ("$(wildcard local.mk)","")
include local.mk
endif

PACKAGE_NAME = gtirb-muse
BINS ?= limit gtirb-muse-server
TEST_ARTIFACTS ?= bin/limit

.DEFAULT_GOAL := all
all: $(addprefix bin/, $(BINS))

include .cl-make/cl.mk

bin/limit:
	gcc -O3 bin/limit.c -o $@
