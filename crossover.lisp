;;; Crossover
(defpackage :gtirb-muse/crossover
  (:use :gt/full
        :functional-trees/attrs
        :software-evolution-library/software-evolution-library
        :software-evolution-library/software/c
        :software-evolution-library/software/parseable
        :software-evolution-library/software/tree-sitter
        :gtirb-muse/ast-utilities
        :gtirb-muse/mutations
        :gtirb-muse/readtable)
  (:export :*crossover-recursive-chance*
           :*crossover-selection-function*))
(in-package :gtirb-muse/crossover)
(in-readtable gtirb-muse-readtable)

(defvar *crossover-recursive-chance* 0.5
  "Probability of crossing over a function AND its function dependencies.")

(defvar *crossover-selection-function* #'random-elt
  "Function used to select an AST from a pool for crossover.")

(defvar *pad-ast* (convert 'c-ast `((:class . :number-literal) (:text . "0")))
  "Padding AST for extra parameters.")

(defmethod crossover ((a c) (b c) &aux (root-a (genome a)) (root-b (genome b)))
  "Perform crossover of top-level function(s) between two software objects,
returning the new software object as the result.  If an error occurs, return
nil."
  (with-attr-table root-b
    (let* ((crossover-fns (select-crossover-functions root-a root-b))
           (crossover-globals (nest (global-declarations-utilized crossover-fns)
                                    (global-declarations root-b))))
      (if crossover-fns
          (nest (copy a :genome)
                (handle-used-parameter-mismatches root-a)
                (handle-unused-parameter-mismatches root-a)
                (reduce (lambda (root crossover-fn)
                          (funcall crossover-fn root))
                        (list {perform-function-crossover _
                               crossover-fns
                               (top-level-functions root-a)}
                              {perform-global-crossover _
                               crossover-globals
                               (global-declarations root-a)}
                              {perform-header-crossover _
                               (headers root-b)
                               (headers root-a)}
                              {crossover-dependencies
                               #'perform-function-declaration-crossover
                               #'function-declarations
                               {function-declaration-dependencies crossover-fns}
                               root-b}
                              {crossover-dependencies
                               #'perform-type-crossover
                               #'type-declarations
                               {type-declarations-utilized crossover-fns
                                                           crossover-globals}
                               root-b})
                        :initial-value root-a))
          (copy a)))))

(-> select-crossover-functions (root-ast root-ast) (values list &optional))
(defun select-crossover-functions (root-a root-b)
  "Return the functions in ROOT-B to crossover into ROOT-A."
  (when-let* ((functions-a (top-level-functions root-a))
              (functions-b (top-level-functions root-b))
              (pool (crossover-pool functions-a functions-b)))
    (if (< *crossover-recursive-chance* (random 1.0))
        (list (funcall *crossover-selection-function* pool))
        (function-dependencies (funcall *crossover-selection-function* pool)
                               functions-b))))

(-> crossover-pool (list list) (values list &optional))
(defun crossover-pool (functions-a functions-b)
  "Return the pool of functions in FUNCTIONS-B that have a corresponding
existing definition in FUNCTIONS-A."
  (nest (remove-if #'null)
        (mapcar {find _ functions-b :test #'name-equal}
                functions-a)))

(-> crossover-dependencies (function function function root-ast root-ast)
                           (values root-ast &optional))
(defun crossover-dependencies (crossover-fn
                               values-fn
                               values-utilized-fn
                               root-b root-a)
  "Helper function for crossing over function declaration/type
dependencies utilized in ROOT-B to ROOT-A."
  (funcall crossover-fn
           root-a
           (funcall values-utilized-fn (funcall values-fn root-b))
           (funcall values-fn root-a)))

(-> perform-function-crossover (root-ast list list) (values root-ast &optional))
(defun perform-function-crossover (root
                                   crossover-functions
                                   existing-functions)
  "Crossover CROSSOVER-FUNCTIONS with EXISTING-FUNCTIONS in ROOT."
  (perform-asts-crossover root
                          crossover-functions
                          existing-functions
                          :replacep t
                          :insert-after nil))

(-> perform-function-declaration-crossover (root-ast list list)
                                           (values root-ast &optional))
(defun perform-function-declaration-crossover (root
                                               crossover-function-declarations
                                               existing-function-declarations)
  "Crossover CROSSOVER-FUNCTION-DECLARATIONS with EXISTING-FUNCTION-DECLARATIONS
in ROOT."
  (perform-asts-crossover root
                          crossover-function-declarations
                          existing-function-declarations
                          :test #'declaration-names-intersection
                          :replacep t
                          :insert-after t))

(-> perform-global-crossover (root-ast list list) (values root-ast &optional))
(defun perform-global-crossover (root crossover-globals existing-globals)
  "Crossover CROSSOVER-GLOBALS variables with EXISTING-GLOBALS in ROOT."
  (perform-asts-crossover root
                          crossover-globals
                          existing-globals
                          :test #'declaration-names-intersection
                          :replacep nil
                          :insert-after t))

(-> perform-type-crossover (root-ast list list) (values root-ast &optional))
(defun perform-type-crossover (root crossover-types existing-types)
  "Crossover CROSSOVER-TYPES with EXISTING-TYPES in ROOT."
  (perform-asts-crossover root
                          crossover-types
                          existing-types
                          :replacep nil
                          :insert-after t))

(-> perform-header-crossover (root-ast list list) (values root-ast &optional))
(defun perform-header-crossover (root crossover-headers existing-headers)
  "Crossover CROSSOVER-HEADERS with EXISTING-HEADERS in ROOT."
  (perform-asts-crossover root
                          crossover-headers
                          existing-headers
                          :replacep nil
                          :insert-after t))

(-> perform-asts-crossover (root-ast list list
                            &key (:test function)
                                 (:replacep boolean)
                                 (:insert-after boolean))
                           (values root-ast &optional))
(defun perform-asts-crossover (root crossover-asts existing-asts
                               &key (test #'name-equal) replacep insert-after)
  "Perform ASTs crossover, adding CROSSOVER-ASTS to ROOT before or after
EXISTING-ASTS (depending on the value of INSERT-AFTER) if no corresponding
EXISTING-AST can be found using TEST.  If the corresponding EXISTING-AST is
found and REPLACEP is non-nil, the EXISTING-AST is replaced by the
CROSSOVER-AST."
  (reduce (lambda (root crossover-ast)
            (perform-ast-crossover root
                                   crossover-ast
                                   existing-asts
                                   :test test
                                   :replacep replacep
                                   :insert-after insert-after))
          (sort-crossover-asts crossover-asts
                               root
                               existing-asts
                               :test test
                               :insert-after insert-after)
          :initial-value root))

(-> perform-ast-crossover (root-ast ast list
                           &key (:test function)
                                (:replacep boolean)
                                (:insert-after boolean))
                          (values root-ast &optional))
(defun perform-ast-crossover (root crossover-ast existing-asts
                              &key (test #'name-equal) replacep insert-after)
  "Perform AST crossover, adding CROSSOVER-AST to ROOT before or after
EXISTING-ASTS (depending on the value of INSERT-AFTER) if no corresponding
EXISTING-AST can be found using TEST.  If the corresponding EXISTING-AST is
found and REPLACEP is non-nil, the EXISTING-AST is replaced by the
CROSSOVER-AST."
  (let* ((existing-ast (find crossover-ast existing-asts :test test))
         (crossover-ast (tree-copy crossover-ast)))
    (cond ((and existing-ast replacep)
           (with root existing-ast crossover-ast))
          ((not existing-ast)
           (nest (insert-empty-statement-if-needed crossover-ast)
                 (if (and existing-asts insert-after)
                     (insert-after root
                                   (lastcar existing-asts)
                                   crossover-ast)
                     (insert root
                             (or (first existing-asts)
                                 (first (children root)))
                             crossover-ast))))
          (t (copy root)))))

(-> sort-crossover-asts (list root-ast list
                         &key (:test function) (:insert-after boolean))
                        (values list &optional))
(defun sort-crossover-asts (crossover-asts root existing-asts
                            &key (test #'name-equal) insert-after)
  "Sort CROSSOVER-ASTS by finding the corresponding EXISTING-AST in ROOT
using TEST and sorting by existing location. CROSSOVER-ASTS with no
corresponding EXISTING-AST will be placed at beginning of the list
and sorted for insertion depending on the value of INSERT-AFTER."
  (multiple-value-bind (found-asts missing-asts)
      (partition {find _ existing-asts :test test} crossover-asts)
    (append (if insert-after missing-asts (reverse missing-asts))
            (stable-sort found-asts
                         (lambda (ast-1 ast-2)
                           (path-later-p root
                                         (ast-path root ast-1)
                                         (ast-path root ast-2)))))))

(-> insert-after (root-ast ast ast) (values root-ast &optional))
(defun insert-after (root pt ast)
  "Insert AST after PT in ROOT.  PT must be a direct child of ROOT."
  (labels ((next-non-empty-child-path (pt &aux (path (ast-path root pt)))
             (assert (length= path 1) ()
                     "insert-after only works on top-level ASTs.")
             (destructuring-bind (slot-name . index)
                 (lastcar path)
               (iter (for i from (1+ index) to (length (direct-children root)))
                     (let ((new-path (list (cons slot-name i))))
                       (unless (typep (lookup root new-path) 'c-empty-statement)
                         (return new-path)))))))
    (if-let ((next-child-path (next-non-empty-child-path pt)))
      (insert root next-child-path ast)
      (insert (with root pt ast) ast pt))))

(-> insert-empty-statement-if-needed (ast root-ast)
                                     (values root-ast &optional))
(defun insert-empty-statement-if-needed (crossover-ast root)
  "Insert an empty statement AST into ROOT after CROSSOVER-AST if
CROSSOVER-AST is of a type which requires an empty statement to be added."
    (if (or (typep crossover-ast 'composite-type-ast)
            (typep crossover-ast 'c-enum-specifier))
        (insert-after root
                      crossover-ast
                      (make 'c-empty-statement :text ";"))
        root))

(-> handle-used-parameter-mismatches (root-ast root-ast)
                                     (values root-ast &optional))
(defun handle-used-parameter-mismatches (root-a root-b)
  "Handle mismatches in function parameters between ROOT-A and ROOT-B by
padding or removing arguments for function callsites in ROOT-B."
  (let* ((functions-a (top-level-functions root-a))
         (functions-b (top-level-functions root-b))
         (crossover-fns (set-difference functions-b functions-a)))
    (reduce (lambda (root mismatched-functions)
              (apply #'fixup-call-arguments-arity root mismatched-functions))
            (parameter-arity-mismatches crossover-fns functions-a)
            :initial-value root-b)))

(-> fixup-call-arguments-arity (root-ast function-ast function-ast)
                               (values root-ast &optional))
(defun fixup-call-arguments-arity (root function-a function-b)
  "Handle mismatches in function parameter arity between FUNCTION-A
and FUNCTION-B by padding or removing arguments for function callsites
for FUNCTION-A in ROOT to match the arity of FUNCTION-B."
  (let* ((num-params-a (length (function-parameters function-a)))
         (num-params-b (length (function-parameters function-b)))
         (indices (nest (take (- (max num-params-b num-params-a)
                                 (min num-params-a num-params-b)))
                        (shuffle)
                        (iota (if (< num-params-a num-params-b)
                                  num-params-b
                                  (1+ num-params-b))
                              :start 0))))
    (reduce (lambda (root callsite)
              (if (and (not (variadic-parameters-p function-a))
                       (not (variadic-parameters-p function-b)))
                  (apply #'callsite-cut-or-pad-arguments
                         (if (< num-params-a num-params-b)
                             #'cut-argument
                             #'pad-argument)
                         root callsite indices)
                  root))
            (call-arguments-arity-mismatches root function-a function-b)
            :initial-value root)))

(-> call-arguments-arity-mismatches (root-ast function-ast function-ast)
                                    (values list &optional))
(defun call-arguments-arity-mismatches (root function-a function-b)
  "Return a list of callsite ASTs for FUNCTION-A in ROOT-B which have
a different arity than FUNCTION-B."
  (nest (remove-if-not [{length= (function-parameters function-b)}
                        #'call-arguments])
        (function-callsites root function-a)))

(-> callsite-cut-or-pad-arguments (function root-ast call-ast &rest fixnum)
                                  (values root-ast &optional))
(defun callsite-cut-or-pad-arguments (op root callsite &rest indices)
  "Apply OP to the CALLSITE in ROOT to either cut or pad callsite arguments
at INDICES, returning the new root."
  (reduce (lambda (root index)
            (funcall op root callsite index))
          (sort indices #'>)
          :initial-value root))

(-> cut-argument (root-ast call-ast array-index) (values root-ast &optional))
(defun cut-argument (root callsite index)
  "Cut the INDEXth argument at CALLSITE in ROOT."
  (less root (nth index (call-arguments callsite))))

(-> pad-argument (root-ast call-ast array-index) (values root-ast &optional))
(defun pad-argument (root callsite index)
  "Pad CALLSITE in ROOT with an additional argument at INDEX."
  (let ((arguments (call-arguments callsite))
        (value (tree-copy *pad-ast*)))
    (if (length< index arguments)
        (insert root (nth index arguments) value)
        (insert (with root (lastcar arguments) value)
                value
                (lastcar arguments)))))

(-> handle-unused-parameter-mismatches (root-ast root-ast)
                                       (values root-ast &optional))
(defun handle-unused-parameter-mismatches (root-a root-b)
  "Handle mismatches in function parameters between ROOT-A and ROOT-B
by removing unused parameters from ROOT-B, if possible."
  (let* ((functions-a (top-level-functions root-a))
         (functions-b (top-level-functions root-b))
         (crossover-fns (set-difference functions-b functions-a)))
    (reduce (lambda (root target)
              (nest (apply-mutation root)
                    (at-targets (make 'remove-unused-parameter))
                    (list target)))
            (nest (mapcar #'car)
                  (remove-if-not «and [#'unused-parameters #'car]
                                      {apply #'parameter-arity->}»)
                  (parameter-arity-mismatches crossover-fns functions-a))
            :initial-value root-b)))
